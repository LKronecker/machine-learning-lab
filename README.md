# Machine learning lab
Machine learning experimentation laboratory. :P :)

## Getting Started

To get the project up and running in your local machine run:
```
git clone https://github.com/JoMedia/machine_learning.git
```
In your commandline tool at the desired file path. Once the project is fully downloaded 

### Prerequisites

To run the project you need to install python 3.6 or higher. It is recommended to use Anaconda as an environment manager.

* [Anaconda](https://www.anaconda.com/download) - Anaconda 

If you want to manage your python packages installation without Anaconda you can install directly on your system 
following the following guides:

* [Installing Python 3 on Mac OS X](http://docs.python-guide.org/en/latest/starting/install3/osx/) - Mac OS X 
* [Installing Python 3 on Windows](http://docs.python-guide.org/en/latest/starting/install3/win/) - Windows 

### Installing

Once you have Python 3 installed in your system we will proceed to installing the required packages for running the project.

The minimum python packages that are required by the project are described in the requirements.txt file.

If you want to install them directly in your system run the following command in your terminal:

```
pip install -r /path/to/requirements.txt
```

If you are using Anaconda as a package managing tool activate the environment destined to be used for this project and 
then run the following command in your command line tool:

```
conda env create -f environment.yml
```

Note: If you are working using an Anaconda environment you will need to tell you IDE to load that specific environment 
so that the packages that you install on it are 'seen' by your IDE. 

For more information:

* [Managing environments](https://conda.io/docs/user-guide/tasks/manage-environments.html)  


Any one of the scripts in the 'experimentation' sections can be run from within any IDE or directly in the command line 
tool by executing them directly as a python script:

```
python3 path_to/<script-name>.py
```

The use of Pycharm IDE is strongly recommended for running the project since it provides all the necessary tools.

* [Download PyCharm](https://www.jetbrains.com/pycharm/download/)  


## Training and optimization

For details on training and optimization procedures take a look at the independent readme files.

- For sffiliate desirability check out the file at path: '/machine_learning/affiliate-desirability/deployment/hyuna_desirability_predictor/readme.txt'
- For super user classification check out te file at path: '/machine_learning/superuser-classification/deployment/su_model_api.md'

## Deployment

In order to deploy the different machine learning models defined by this project the following steps must be followed:

- Train a specific model using the scripts (or creating new if necessary) defined in the experimentation section of the 
problem in question.

- Once the desired accuracy (recall and sensitivity) are achieved save the model in pickle format.Tis is done 
automatically by running the script that trains the desired model. The function that 
persists the model in pickle format if called 'save_model_new' and is located inside the result_creator.py module.

- Move the saved model to the deployment section of the problem in question into the folder defined by the following 
path '~/deployment/container/model-name/model'

- Copy one of the already working Docker files into to path: '~/machine_learning/problem-name/deployment/container'

- Copy one of the already working deployment scripts 'build_and_push' into the same path, change the image name inside 
the script to describe the new model that will be deployed (image=pl3-'model-name'-us-east-2-q1) and execute it by copy 
pasting the following command into your command line tool:
```
./build_and_push.sh
```

Following the above steps will push a docker image to the amazon docker registry.

## Running the tests

No unit test are added yet.
