# preprocessing
from superuser_preprocessing import *
# Metrics
from metrics import *
# Results
from result_creator import *
# Model implementation
from sklearn.ensemble import RandomForestClassifier
# Feature influence visualization
from helpers import plot_feature_ranking
#helpers
from pprint import pprint as pp
import numpy as np

seed = 132069
# Seed
np.random.seed(seed)

# Hyperparameters
n_estimators = 520
max_depth = 2
max_features = 'sqrt'
random_state = seed
criterion = 'entropy'
bootstrap = True
prediction_threshold = .6

# Optimization
normalize_features = False
use_pca = False


features = ["Started_Audiobooks_10", "Consumed_Audiobooks_10", "Invoice_Cost_10", "Avg_Cost_Read_10"]


def train_model(features_train, labels_train):
    random_forest = RandomForestClassifier(n_estimators=n_estimators, max_features=max_features, criterion=criterion,
                                           max_depth=max_depth, random_state=random_state, bootstrap=bootstrap)
    random_forest.fit(features_train, labels_train)
    return random_forest


def create_predictions(model, features_test):
    probabilities = create_probability_distributions(model, features_test)
    predictions_filtered = np.array([1 if probabilities[:, 1][k] > prediction_threshold else 0 for k in
                                     range(len(probabilities[:, 1]))])
    return predictions_filtered


def create_probability_distributions(model, features_test):
    probabilities = model.predict_proba(features_test)
    return probabilities


def evaluate_model_performance(model, predictions, labels_test, save_results_file=False):
    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(predictions, labels_test)
    roc_auc = area_under_curve(predictions, labels_test)

    # Print metrics
    print('\nRandom Forests - Testing Confusion Matrix\n\n', pd.crosstab(labels_test, predictions,
                                                                         rownames=['Actuall'], colnames=['Predicted']))

    print('\nRandom Forests - Clasification report\n', classification_report(labels_test, predictions))

    print('\nRandom Forests - Test Accuracy\n', round(accuracy_score(labels_test, predictions), 3))

    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['model_name'] = str(type(model))
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['features_used'] = features
    results_dictionary['hyperparameters'] = {'number_of_trees': n_estimators, 'seed': seed}
    results_dictionary['precision'] = precision
    results_dictionary['recall/sensitivity'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold
    results_dictionary['area_under_curve'] = roc_auc

    pp(results_dictionary)

    if save_results_file == True:
        create_results_file(file_name, results_dictionary)


def main():
    # Change training set if desired here
    train_dataFrame = pd.read_csv('~/machine_learning/superuser-classification/experimentation/data/10-days/sup_train_dataset_copy.csv')
    # CHange testing set if desired here
    test_dataFrame = pd.read_csv('~/machine_learning/superuser-classification/experimentation/data/10-days/sup_test_dataset_copy.csv')

    # Create test and train sets
    features_train, labels_train, feature_list = create_trainning_set(features, train_dataFrame)
    features_test, labels_test = create_testing_set(features, test_dataFrame)

    # Train model
    model = train_model(features_train, labels_train)
    # Create predictions
    predictions = create_predictions(model, features_test)

    plot_feature_ranking(model, features_train, features)

    evaluate_model_performance(model, predictions, labels_test, save_results_file=False)

    # Saves the model to the disc
    save_model_new(model, 'model_'+str(type(model)))


if __name__ == '__main__':
    main()
