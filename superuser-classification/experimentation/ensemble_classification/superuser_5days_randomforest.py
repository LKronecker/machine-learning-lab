# preprocessing
from superuser_preprocessing import *
# Metrics
from metrics import *

# Results
from result_creator import *

import numpy as np

# Model implementation
from sklearn.ensemble import RandomForestClassifier

from helpers import plot_feature_ranking

#helpers
from pprint import pprint as pp

#PCA
from sklearn.decomposition import PCA

normalize_features = False
use_pca = False
seed = 132069
prediction_threshold = .6
np.random.seed(seed)

#Hyperparameters

n_estimators = 520
max_depth = 2
max_features = 'sqrt'
random_state = seed
criterion = 'entropy' # ginni
bootstrap = True


features = ["Consumed_Audiobooks_5", "Started_Audiobooks_5", "Invoice_Cost_5"]
# , "Consumed_1_5", "Total_Seconds_5",
#             "Perc_Day_5","Start_End_5","Avg_Cost_Read_5"

# features = ['Consumed_Audiobooks_5', 'Invoice_Cost_5', 'Started_Audiobooks_5', 'Started_Audiobooks_4', 'Consumed_Audiobooks_4',
#        'Consumed_1_5', 'Invoice_Cost_4', 'Total_Seconds_5', 'Perc_Day_5']

# features = ['monthly_revenue','Started_Audiobooks_1', 'Started_Audiobooks_2', 'Started_Audiobooks_3',
#        'Started_Audiobooks_4', 'Started_Audiobooks_5', 'Started_1_5',
#        'Consumed_Audiobooks_1', 'Consumed_Audiobooks_2',
#        'Consumed_Audiobooks_3', 'Consumed_Audiobooks_4',
#        'Consumed_Audiobooks_5', 'Consumed_1_5', 'Consumed_Changes_1_5',
#        'Consumed_1vs5', 'Ended_Audiobooks_1', 'Ended_Audiobooks_2', 'Ended_Audiobooks_3',
#        'Ended_Audiobooks_4', 'Ended_Audiobooks_5', 'Ended_1_5',
#        'Ended_Changes_1_5', 'Total_Seconds_1', 'Total_Seconds_2',
#        'Total_Seconds_3', 'Total_Seconds_4', 'Total_Seconds_5', 'Perc_Day_1', 'Perc_Day_2',
#        'Perc_Day_3', 'Perc_Day_4', 'Perc_Day_5', 'Completion_Rate_1',
#        'Completion_Rate_2', 'Completion_Rate_3', 'Completion_Rate_4',
#        'Completion_Rate_5', 'Invoice_Cost_1', 'Invoice_Cost_2',
#        'Invoice_Cost_3', 'Invoice_Cost_4', 'Invoice_Cost_5', 'Avg_Cost_Read_1',
#        'Avg_Cost_Read_2', 'Avg_Cost_Read_3', 'Avg_Cost_Read_4',
#        'Avg_Cost_Read_5']


def train_model(features_train, labels_train):
    random_forest = RandomForestClassifier(n_estimators=n_estimators, max_features=max_features, criterion=criterion,
                                           max_depth=max_depth, random_state=random_state, bootstrap=bootstrap)
    random_forest.fit(features_train, labels_train)
    return random_forest


def create_predictions(model, features_test):
    probabilities = create_probability_distributions(model, features_test)
    predictions_filtered = np.array([1 if probabilities[:, 1][k] > prediction_threshold else 0 for k in
                                     range(len(probabilities[:, 1]))])
    return predictions_filtered


def create_probability_distributions(model, features_test):
    probabilities = model.predict_proba(features_test)
    return probabilities


def normalize_features(train_dataFrame, test_dataFrame):

    list_of_features_to_normalize_together = ['Consumed_Audiobooks_1', 'Consumed_Audiobooks_2', 'Consumed_Audiobooks_3',
                                              'Consumed_Audiobooks_4', 'Consumed_Audiobooks_5']

    # Normalization step
    grouped_features_max, max_per_feature = calculate_normalization_coefficients(train_dataFrame, features,
                                                                                 list_of_features_to_normalize_together)

    train_dataFrame = normalize_values(train_dataFrame, features, list_of_features_to_normalize_together,
                                       grouped_features_max, max_per_feature)

    test_dataFrame = normalize_values(test_dataFrame, features, list_of_features_to_normalize_together,
                                      grouped_features_max, max_per_feature)

    return train_dataFrame, test_dataFrame


def apply_PCA(X):
    pca = PCA(n_components=2)
    pca.fit(X)
    transformed_set = pca.transform(X)
    return transformed_set


def evaluate_model_performance(model, predictions, labels_test, save_results_file=False):
    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(predictions, labels_test)
    roc_auc = area_under_curve(predictions, labels_test)

    # Print metrics
    print('\nRandom Forests - Testing Confusion Matrix\n\n', pd.crosstab(labels_test, predictions,
                                                                         rownames=['Actuall'], colnames=['Predicted']))

    print('\nRandom Forests - Clasification report\n', classification_report(labels_test, predictions))

    print('\nRandom Forests - Test Accuracy\n', round(accuracy_score(labels_test, predictions), 3))

    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['model_name'] = str(type(model))
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['features_used'] = features
    results_dictionary['hyperparameters'] = {'number_of_trees': n_estimators, 'seed': seed}
    results_dictionary['precision'] = precision
    results_dictionary['recall/sensitivity'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold
    results_dictionary['area_under_curve'] = roc_auc

    pp(results_dictionary)

    if save_results_file == True:
        create_results_file(file_name, results_dictionary)



def main():
    train_dataFrame = pd.read_csv('~/machine_learning/superuser-classification/experimentation/data/clean_data_frame2.csv')
    # test_dataFrame = pd.read_csv('~/machine_learning/superuser-classification/experimentation/data/test_dataset2.csv')

    # 10 days testing set
    test_dataFrame = pd.read_csv(
        '~/machine_learning/superuser-classification/experimentation/data/10-days/sup_test_dataset_copy.csv')

    if normalize_features is True:
        train_dataFrame, test_dataFrame = normalize_features(train_dataFrame, test_dataFrame)


    # Create test and train sets
    features_train, labels_train, feature_list = create_trainning_set(features, train_dataFrame)
    features_test, labels_test = create_testing_set(features, test_dataFrame)


    if use_pca == True:
        features_train = apply_PCA(features_train)
        features_test = apply_PCA(features_test)


    model = train_model(features_train, labels_train)
    predictions = create_predictions(model, features_test)

    plot_feature_ranking(model, features_train, features)

    evaluate_model_performance(model, predictions, labels_test, save_results_file=True)

    # save_model_new(model, 'model_'+str(type(model)))


if __name__ == '__main__':
    main()

