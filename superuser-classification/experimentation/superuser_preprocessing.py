# Data frame and processing
import pandas as pd
from sklearn.utils import shuffle


def create_trainning_set(feature_list, base_dataFrame):

    # Create super user dataframe Ord : (3205, 91)
    superuser_dataFrame = base_dataFrame.loc[base_dataFrame['Super_User'] == 1]
    superuser_dataFrame = shuffle(superuser_dataFrame)

    # Create NON super user dataframe  Ord: (15611, 91)
    NONsuperuser_dataFrame = base_dataFrame.loc[base_dataFrame['Super_User'] == 0]
    NONsuperuser_dataFrame = shuffle(NONsuperuser_dataFrame)
    # Reshape DF to match the number of samples of superuser df Ord : (3205, 91)
    NONsuperuser_dataFrame = NONsuperuser_dataFrame[0:superuser_dataFrame.shape[0]]

    # Concatenate and randomize
    training_dataFrame = pd.concat([superuser_dataFrame, NONsuperuser_dataFrame])
    training_dataFrame = shuffle(training_dataFrame)

    # training_dataFrame.replace('Yes', 1., inplace=True)
    # training_dataFrame.replace('No', 0., inplace=True)

    features_train = training_dataFrame[list(feature_list)].values
    labels_train = training_dataFrame['Super_User'].values

    return features_train, labels_train, feature_list


def create_testing_set(feature_list, test_dataFrame):

    # test_dataFrame.replace('Yes', 1, inplace=True)
    # test_dataFrame.replace('No', 0, inplace=True)

    features_test = test_dataFrame[list(feature_list)].values
    # create labels for inputing to the model. Ord: (6410,)
    labels_test = test_dataFrame['Super_User'].values

    return features_test, labels_test


# For neural network
def create_training_and_validation_sets(feature_list, base_dataFrame):

    # replace string 'Yes' for 1 and 'No' for 0
    # base_dataFrame.replace('Yes', 1., inplace=True)
    # base_dataFrame.replace('No', 0., inplace=True)

    # Filter su into one df. Ord : (3205, len(features))
    su_training_set = base_dataFrame.loc[base_dataFrame['Super_User'] == 1]
    su_training_set = shuffle(su_training_set)
    # Create su validation df. Ord : (641, len(features))
    su_validation_set = su_training_set[0:int(su_training_set.shape[0]*.2)]
    #Shuffle
    su_training_set = shuffle(su_training_set)
    su_validation_set = shuffle(su_validation_set)

    # Filter nonsu into one df. Ord : (15611, len(features))
    nonsu_training_set = base_dataFrame.loc[base_dataFrame['Super_User'] == 0]
    nonsu_training_set = shuffle(nonsu_training_set)
    # Create nonsu validation df by taking the last validation_su_dataset.shape[0] elements. Ord : (641, len(features))
    nonsu_validation_set = nonsu_training_set.tail(su_validation_set.shape[0])
    # Resize it to match training_su_dataFrame : Ord : (3205, len(features))
    nonsu_training_set = nonsu_training_set[0:su_training_set.shape[0]]

    # Concatenate and randomize training set
    training_set = pd.concat([su_training_set, nonsu_training_set])
    training_set = shuffle(training_set)
    # Concatenate and randomize validation set
    validation_set = pd.concat([su_validation_set, nonsu_validation_set])
    validation_set = shuffle(validation_set)

    features_train = training_set[list(feature_list)].values
    labels_train = training_set['Super_User'].values

    features_validation = validation_set[list(feature_list)].values
    labels_validation = validation_set['Super_User'].values

    return features_train, labels_train, features_validation, labels_validation


def create_testing_set_for_nn(feature_list, test_dataFrame):

    test_dataFrame.replace('Yes', 1., inplace=True)
    test_dataFrame.replace('No', 0., inplace=True)

    features_test = test_dataFrame[list(feature_list)].values
    # create labels for inputing to the model. Ord: (6410,)
    labels_test = test_dataFrame['Super_User'].values

    return features_test, labels_test


def calculate_normalization_coefficients(base_dataFrame, feature_list, list_of_features_to_normalize_together):
    grouped_features_max = .0
    max_per_feature = .0

    if list_of_features_to_normalize_together:
        # Calculate overall maximum
        list_of_maximums = []
        for feature_name in list_of_features_to_normalize_together:
            list_of_maximums.append(max(list(base_dataFrame[feature_name].values)))

        grouped_features_max = max(list_of_maximums)

    list_of_features_to_normalize_separately = list(set(feature_list).difference(set(list_of_features_to_normalize_together)))

    for feature_name in list_of_features_to_normalize_separately:
        max_per_feature = max(list(base_dataFrame[feature_name].values))

    return grouped_features_max, max_per_feature


def normalize_values(base_dataFrame, feature_list, list_of_features_to_normalize_together, grouped_features_max, max_per_feature):
    if list_of_features_to_normalize_together:
        # Normalize features wit overall maximum
        for feature_name in list_of_features_to_normalize_together:
            base_dataFrame[feature_name] = base_dataFrame[feature_name].apply(lambda x: x / grouped_features_max)

    list_of_features_to_normalize_separately = list(
        set(feature_list).difference(set(list_of_features_to_normalize_together)))

    for feature_name in list_of_features_to_normalize_separately:
        base_dataFrame[feature_name] = base_dataFrame[feature_name].apply(lambda x: x / max_per_feature)

    return base_dataFrame
