# Dataset
from superuser_preprocessing import *

#model
from sklearn import linear_model
from sklearn.metrics import explained_variance_score, mean_absolute_error, mean_squared_error

#helpers
from pprint import pprint as pp

def train_model(features_train, labels_train):
    reg_model = linear_model.LinearRegression()
    reg_model.fit(features_train, labels_train)
    return reg_model


def create_predictions(model, features_test):
    predictions = model.predict(features_test)
    return predictions


def evaluate_predictions(predictions, labels_test):

    exp_var_score = explained_variance_score(labels_test, predictions)
    print(exp_var_score)
    mn_abs_err = mean_absolute_error(labels_test, predictions)
    print(mn_abs_err)
    mn_sqr_err = mean_squared_error(labels_test, predictions)
    print(mn_sqr_err)

def main():

    features= ["Started_Audiobooks_5", "Consumed_Audiobooks_5", "Consumed_1_5", "Total_Seconds_5", "Start_End_5", "Perc_Day_5", "Invoice_Cost_5", "Avg_Cost_Read_5"]
    #features = ["Invoice_Cost_5", "Started_Audiobooks_5", "Total_Seconds_5", "Perc_Day_5", "Start_End_5", "Avg_Cost_Read_5", "Avg_Start_End_5"]

    # Create test and train sets
    features_train, labels_train, feature_list = create_trainning_set(features)
    features_test, labels_test = create_testing_set(features)

    model = train_model(features_train, labels_train)
    predictions = create_predictions(model, features_test)

    evaluate_predictions(predictions, labels_test)


if __name__ == '__main__':
    main()