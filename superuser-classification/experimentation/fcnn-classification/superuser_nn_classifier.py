# Dataset
from superuser_preprocessing import *

import numpy as np

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras import optimizers

# Results
from result_creator import *
# Metrics
from metrics import *

#helpers
from pprint import pprint as pp
import datetime

# global variables
number_of_features = int()
num_classes = 2
seed = 42

np.random.seed(seed)

# Training Hyperparameters
learning_rate = 0.01
minibatch_size = 20
num_epochs = 100

# Model Hyperparameters :
internal_activation = 'relu'
output_activation = 'softmax'

# Optimizer
loss_function = 'categorical_crossentropy'
metrics = ['accuracy']

prediction_threshold = .7


def build_model(input_dim):
    model = Sequential()

    model.add(Dense(256, activation=internal_activation, input_dim=input_dim))
    model.add(Dropout(0.5))

    model.add(Dense(128, activation=internal_activation))
    model.add(Dropout(0.5))

    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(num_classes, activation=output_activation))
    model.summary()

    optimizer = optimizers.RMSprop(lr=learning_rate)
    # optimizer = optimizers.Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss=loss_function, optimizer=optimizer, metrics=metrics)

    return model, optimizer


def train_model(model, x_train, y_train, x_val, y_val):
    model.fit(x_train, y_train, batch_size=minibatch_size, epochs=num_epochs, validation_data=(x_val, y_val), verbose=2)


def evaluate_model(model, x_test, y_test):
    score = model.evaluate(x_test, y_test, verbose=0)
    return score


def predict(model, x_test):
    predictions = model.predict(x_test)
    return  predictions

def main():
    ### Normalize dataframes
    #base_original_dataFrame = pd.read_csv('~/Prototypes/machine-learning-lab/superuser-classification/data/sup_main_dataset.csv')

    base_dataFrame = pd.read_csv('~/machine_learning/superuser-classification/experimentation/data/clean_data_frame.csv')
    test_dataFrame = pd.read_csv('~/machine_learning/superuser-classification/experimentation/data/test_dataset.csv')

    feature_list = ["Started_Audiobooks_5", 'Consumed_Audiobooks_1', 'Consumed_Audiobooks_2',
                    'Consumed_Audiobooks_3', 'Consumed_Audiobooks_4','Consumed_Audiobooks_5', "Total_Seconds_5",
                    "Start_End_5", "Perc_Day_5", "Invoice_Cost_5", "Avg_Cost_Read_5"]

    # first normalize values
    list_of_features_to_normalize_together = ['Consumed_Audiobooks_1', 'Consumed_Audiobooks_2', 'Consumed_Audiobooks_3',
                                              'Consumed_Audiobooks_4', 'Consumed_Audiobooks_5']

    grouped_features_max, max_per_feature = calculate_normalization_coefficients(base_dataFrame, feature_list,
                                                                                 list_of_features_to_normalize_together)

    base_dataFrame = normalize_values(base_dataFrame, feature_list, list_of_features_to_normalize_together,
                                      grouped_features_max, max_per_feature)

    test_dataFrame = normalize_values(test_dataFrame, feature_list, list_of_features_to_normalize_together,
                                      grouped_features_max, max_per_feature)

    features_train, labels_train, features_validation, labels_validation = create_training_and_validation_sets(feature_list, base_dataFrame)
    # Hot encode labels
    labels_train = keras.utils.to_categorical(labels_train, num_classes)
    labels_validation = keras.utils.to_categorical(labels_validation, num_classes)

    features_test, labels_test = create_testing_set_for_nn(feature_list, test_dataFrame)
    # Hot encode labels
    labels_test = keras.utils.to_categorical(labels_test, num_classes)

    model, optimizer = build_model(len(feature_list))
    train_model(model, features_train, labels_train, features_validation, labels_validation)

    score = evaluate_model(model, features_test, labels_test)
    predictions = predict(model, features_test)

    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(predictions, labels_test[:,1],
                                                                                   prediction_threshold)

    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['Experiment'] = file_name
    results_dictionary['Model name'] = str(type(model))
    results_dictionary['Testing samples'] = len(labels_test)
    results_dictionary['Features used'] = feature_list
    results_dictionary['Training hyperparameters'] = {'num_epochs': num_epochs,
                                                      'seed': seed,
                                                      'learning_rate': learning_rate}

    results_dictionary['Model hyperparameters'] = {'layers': layers_info(model.layers),
                                                   'number of layers': len(model.layers),
                                                   'internal_activation': internal_activation,
                                                   'output_activation': output_activation}

    results_dictionary['Optimizer'] = {'optimizer': str(optimizer),
                                       'loss_function': loss_function,
                                       'metrics': metrics}

    results_dictionary['precision'] = precision
    results_dictionary['recall/sensitivity'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold

    results_dictionary['Score'] = score
    # results_dictionary['Predictions'] = predictions

    pp(results_dictionary)
    create_results_file(file_name, results_dictionary)
    # save_model(model, 'model_' + str(type(model)))


# Result helpers
def layers_info(layers):
    layer_info_dict = {}
    for layer in layers:
        if type(layer) == keras.layers.core.Dense:
            layer_info_dict[layer.name] = {'units': str(layer.units)}
        else:
            layer_info_dict[layer.name] = {'rate': layer.rate}

    return layer_info_dict


if __name__ == '__main__':
    main()
