import predictor as myapp

# This is just a simple wrapper for gunicorn to find our app.
# If you want to change the algorithm file, simply change "predictor.py" above to the
# new file.

app = myapp.app
