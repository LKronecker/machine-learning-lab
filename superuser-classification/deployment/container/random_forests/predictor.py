
from __future__ import print_function

import os
import pickle
from io import StringIO
import flask
from flask import jsonify

import pandas as pd
import numpy as np
from pprint import pprint as pp
# Date formatting
import datetime
now = datetime.datetime.now()

# Path for deployment (specified in docker file)
prefix = '/opt/program/'
model_name = 'random-forest-model.pkl'

# Model path
model_path = os.path.join(prefix, 'model')

# A singleton for holding the model. This simply loads the model and holds it.
# It has a predict function that does a prediction based on the model and the input data.

class ScoringService(object):
    # The following parameters (- model) are specific params that were used to train the model being used. At some point
    # they should be moved to a configuration file of some sort or they could be imported from the module used to train
    # the original model

    # Features used by the corresponding model, must be changed according to the features used to train the model
    features = ["Started_Audiobooks_10", "Consumed_Audiobooks_10", "Invoice_Cost_10", "Avg_Cost_Read_10"]

    # Prediction threshold
    prediction_threshold = .6

    # Where we keep the model when it's loaded
    model = None

    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.model is None:
            # Serialize the model in a format supported by the container's python version or update the python ver.
            with open(os.path.join(model_path, model_name), 'rb') as inp:
                cls.model = pickle.load(inp)
        return cls.model

    @classmethod
    def predict(cls, input):
        """For the input, do the predictions and return them.
        Args:
            input (a pandas dataframe): The data on which to do the predictions. There will be  random-forest-model
                one prediction per row in the dataframe"""

        all_features = list(input.columns)
        probabilities = None
        predictions = None

        if set(cls.features).issubset(all_features):
            input_data = input[list(cls.features)].values
            random_forest_model = cls.get_model()

            probabilities = list(cls.create_probability_distributions(random_forest_model, input_data))
            predictions = list(cls.create_predictions(probabilities))

        return probabilities, predictions

    @classmethod
    def create_probability_distributions(cls, model, features_test):
        probabilities = model.predict_proba(features_test)
        return probabilities[:, 1]

    @classmethod
    def create_predictions(cls, probabilities):
        predictions_filtered = np.array([1 if probabilities[k] > cls.prediction_threshold else 0 for k in
                                     range(len(probabilities))])
        return predictions_filtered

    @classmethod
    def preprocess_data(cls, dataframe):
        dataframe.replace('Yes', 1., inplace=True)
        dataframe.replace('No', 0., inplace=True)

        data = dataframe[list(cls.features)].values
        return data


# The flask app for serving predictions
app = flask.Flask(__name__)


@app.route('/ping', methods=['GET'])
def ping():
    """Determine if the container is working and healthy. In this sample container, we declare
    it healthy if we can load the model successfully."""
    model = ScoringService.get_model()

    if model is not None:
        result = 'Model loaded properly'
        status = 200
    else:
        result = 'Model did not load properly'
        status = 404

    responses = jsonify(model=str(model), result=result)
    responses.status_code = status

    return responses


@app.route('/invocations', methods=['POST'])
def transformation():
    """Do an inference on a single batch of data. We take data as CSV, convert it to a pandas data frame for internal
    use and then convert the predictions back to CSV.
    """
    # Extract data from request
    data = flask.request.data.decode('utf-8')

    # Check if data is present.
    if data is None:
        return flask.Response(response='Empty data', status=415, mimetype='text/plain')

    # Check if data has the right format.
    if flask.request.content_type == 'text/plain' or flask.request.content_type == 'text/csv':
        # Convert from CSV to pandas
        s = StringIO(data)
        data = pd.read_csv(s)

    else:
        # Return error for unsuported data type
        return flask.Response(response='Invalid data format.', status=415, mimetype='text/plain')

    member_ids = [str(id) for id in list(data['member_id'])]

    probabilities, predictions = ScoringService.predict(data)

    # Error due t missing data
    if probabilities is None or predictions is None:
        response_error = "Missing or incomplete data. There's at least one feature (column) missing. Please make sure " \
                         "that your request includes the following data columns: {}".format(ScoringService.features)

        return flask.Response(response=response_error, status=415, mimetype='text/plain')

    # Create lists of probabilities and predictions
    probabilities = [str(prob) for prob in probabilities]
    predictions = [str(pred) for pred in predictions]

    results_dictionary = {'member_ids': member_ids, 'probabilities': probabilities, 'predictions': predictions,
                          'date':now.strftime("%Y-%m-%d %H:%M:%S")}

    responses = jsonify(results_dictionary)
    responses.status_code = 200

    return responses
