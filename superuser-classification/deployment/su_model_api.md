Super User classification model
====================

## Summary

This API provides an interface to interact with the super user classification model.

Base path: https://runtime.sagemaker.us-east-2.amazonaws.com/endpoints/su-classification-model/invocations

| Operation | Description |
|-----------|-------------|
|[POST /invocations](#post-invocations)|Request predictions on a given data set.|

## Paths

### POST /invocations

#### DESCRIPTION
Request predictions on a given data set.

The predictions will be computed on the data set passed in the body of the request. 

The data set that is passed along with the request must be in csv format.

The data set must contain the features (column names) that were used to train the model. 

If no data set is passed the response will provide an empty response.

#### REQUEST BODY
Uses **text/plain**.
##### PROPERTIES
>**data**: *.csv `required` <br>
>The data set to perform the predictions on. Must be formated as a csv file and must contain the following 
columns (with the corresponding data related to them): "Started_Audiobooks_10", "Consumed_Audiobooks_10", 
"Invoice_Cost_10" and "Avg_Cost_Read_10". All of them need to be present for the model to be able to 
perform the prediction.
 <br>

#### AWS SIGNATURE PARAMETERS

| Name | Description | Value | DataType |
|------|-------------|------|----------|
| AccessKey | AWS access key | AKIAIPVTNFZ6UM3KBH2A | string `required` |
| SecretKey | AWS SecretKey | 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 | string `required` |
| AWS Region | AWS Region | us-east-1 | string `required` |
| Service Name | AWS Service Name | sagemaker | string `required` |

#### RESPONSES
Uses **application/json**

**200 OK**

Returns predictions for the data that was passed with the request. The response is a json containning the properties 
descibed bellow. Each one of them is an ordered list and the independent entries are related given the index in the list.

##### PROPERTIES
>**member_ids**: object[] `required` <br>
>**probabilities**: object[] `required` <br>
>**predictions**: object[] `required` <br>
>**date**: string `required` <br>


**415 Empty data**: There was no data set passed so that predictions can be performed. Please 
make sure that your data set is attached to the body of your request.

**415 Invalid data format**: This predictor only supports CSV data

**415 Missing or incomplete data.**: Missing or incomplete data. There's at least one feature (column) missing. Please 
make sure that your request includes the features (data columns) on which the model was trained.

**404 Model did not load properly**: The model did no load properly.

**424 ModelError**: Model (created by playster in the container) returned an error 500.

**400 ValidationError**: Inspect your request and try again.

**400 AccessDeniedException**: You do not have sufficient access to perform this action.

**400 IncompleteSignature**: The request signature does not conform to AWS standards.

**400 InvalidAction**: The action or operation requested is invalid. Verify that the action is typed correctly.

**400 InvalidParameterCombination**: Parameters that must not be used together were used together.

**400 InvalidParameterValue**: An invalid or out-of-range value was supplied for the input parameter.

**400 InvalidQueryParameter**: The AWS query string is malformed or does not adhere to AWS standards.

**400 MissingAction**: The request is missing an action or a required parameter.

**400 MissingParameter**: A required parameter for the specified action is not supplied.

**400 RequestExpired**: The request reached the service more than 15 minutes after the date stamp on the request or more than 15 minutes after the request expiration date (such as for pre-signed URLs), or the date stamp on the request is more than 15 minutes in the future.

**400 ThrottlingException**: The request was denied due to request throttling.

**403 InvalidClientTokenId**: The X.509 certificate or AWS access key ID provided does not exist in our records.

**403 MissingAuthenticationToken**: The request must contain either a valid (registered) AWS access key ID or X.509 certificate.

**403 OptInRequired**: The AWS access key ID needs a subscription for the service.

**404 MalformedQueryString**: The query string contains a syntax error.

**500 InternalFailure**: The request processing has failed because of an unknown error, exception or failure.

**503 ServiceUnavailable**: The request has failed due to a temporary failure of the server. Try your call again.

<br>