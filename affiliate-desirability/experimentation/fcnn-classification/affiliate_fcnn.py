import pandas as pd
import numpy as np

# Metrics
from metrics import *

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras import optimizers

#Preprocessing
from desirability_preprocessing import *

# Results
from result_creator import *

from pprint import pprint as pp
import datetime

num_classes = 2
seed = 42

np.random.seed(seed)

# Training Hyperparameters
learning_rate = 0.01
minibatch_size = 20
num_epochs = 100

prediction_threshold = .5

# Model Hyperparameters :
internal_activation = 'relu'
output_activation = 'softmax'

# Optimizer
loss_function = 'categorical_crossentropy'
metrics = ['accuracy']

def build_model(input_dim):
    model = Sequential()

    model.add(Dense(256, activation='relu', input_dim=input_dim))
    model.add(Dropout(0.5))

    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(num_classes, activation='softmax'))
    model.summary()

    optimizer = optimizers.RMSprop(lr=learning_rate)
    # optimizer = optimizers.Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss=loss_function, optimizer=optimizer, metrics=metrics)

    return model, optimizer


def train_model(model, x_train, y_train, x_val, y_val):
    model.fit(x_train, y_train, batch_size=minibatch_size, epochs=num_epochs, validation_data=(x_val, y_val), verbose=2)


def evaluate_model(model, x_test, y_test):
    score = model.evaluate(x_test, y_test, verbose=0)
    return score


def predict(model, x_test):
    predictions = model.predict(x_test)
    return predictions

def main():
    dataframe_normalized = pd.read_csv(
        '~/machine_learning/affiliate-desirability/experimentation/Main_Normalized_Undesirability_Dataset_2018-05-08.csv')
    dataframe_test_normalized = pd.read_csv(
        '~/machine_learning/affiliate-desirability/experimentation/Test_Normalized_Undesirability_Dataset_2018-05-08.csv')

    # dataframe_normalized = balance_dataset(dataframe_normalized)

    # independent variable = Gross_Margin_126
    features = ["rev_VS_cost_36", "total_cancel_rate_36", "avg_net_rev_36", "Net_Settlement_36_Perc", "Gross_Margin_36",
                "GM_growth_rate_36", "rev_VS_cost_36"]

    dataframe_training = dataframe_normalized[0:int(dataframe_normalized.shape[0] * .8)]
    dataframe_validation = dataframe_normalized[int(dataframe_normalized.shape[0] * .8): int(dataframe_normalized.shape[0])]

    training_set = dataframe_training[list(features)].values
    validation_set = dataframe_validation[list(features)].values
    testing_set = dataframe_test_normalized[list(features)].values

    # Labels
    labels_train = dataframe_training['Undesirable'].values
    labels_validation = dataframe_validation['Undesirable'].values
    labels_test = dataframe_test_normalized['Undesirable'].values

    # Hot encode labels
    labels_train = keras.utils.to_categorical(labels_train, num_classes)
    labels_validation = keras.utils.to_categorical(labels_validation, num_classes)
    labels_test = keras.utils.to_categorical(labels_test, num_classes)

    model, optimizer = build_model(len(features))
    train_model(model, training_set, labels_train, validation_set, labels_validation)

    score = evaluate_model(model, testing_set, labels_test)
    predictions = predict(model, testing_set)

    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(predictions, labels_test[:,1],
                                                                                   prediction_threshold)
    # _, _, _, roc_auc = area_under_curve(predictions, labels_test, prediction_threshold)

    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['Experiment'] = file_name
    results_dictionary['Model name'] = str(type(model))
    results_dictionary['Testing samples'] = len(labels_test)
    results_dictionary['Features used'] = features
    results_dictionary['Training hyperparameters'] = {'num_epochs': num_epochs,
                                                      'seed': seed,
                                                      'learning_rate': learning_rate}

    results_dictionary['Model hyperparameters'] = {'layers': layers_info(model.layers),
                                                   'number of layers': len(model.layers),
                                                   'internal_activation': internal_activation,
                                                   'output_activation': output_activation}

    results_dictionary['Optimizer'] = {'optimizer': str(optimizer),
                                       'loss_function': loss_function,
                                       'metrics': metrics}

    results_dictionary['precision-specificity(tp/tp+fp)'] = precision
    results_dictionary['recall-sensitivity(tp/tp+fn)'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_desc': ['[TN, FP]', '[FN, TP]'],
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold
    results_dictionary['Score'] = score
    # results_dictionary['area_under_curve'] = roc_auc

    pp(results_dictionary)
    # create_results_file(file_name, results_dictionary)
    # save_model(model, 'model_' + str(type(model)))


# Result helpers
def layers_info(layers):
    layer_info_dict = {}
    for layer in layers:
        if type(layer) == keras.layers.core.Dense:
            layer_info_dict[layer.name] = {'units': str(layer.units)}
        else:
            layer_info_dict[layer.name] = {'rate': layer.rate}

    return layer_info_dict


if __name__ == '__main__':
    main()