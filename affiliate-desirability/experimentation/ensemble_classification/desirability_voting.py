#Preprocessing
from desirability_preprocessing import *

# Metrics
from metrics import *

# Model
from sklearn.ensemble import VotingClassifier, RandomForestClassifier, AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

# Results
from result_creator import *

#helpers
from pprint import pprint as pp
import datetime

prediction_threshold = .5
seed = 132069


def train_model(features_train, labels_train, max_features):
    random_forest = RandomForestClassifier(n_estimators=500, max_features=max_features)
    adaboost = AdaBoostClassifier(DecisionTreeClassifier(max_depth=2), algorithm="SAMME", n_estimators=500)

    voting = VotingClassifier(estimators=[('rf', random_forest), ('ab', adaboost)], voting='soft')
    voting = voting.fit(features_train, labels_train)

    return voting


def create_predictions(model, features_test):
    predictions = model.predict_proba(features_test)
    return predictions


def main():
    # Uncomment to renormalize original datasets
    # dataframe_normalized, dataframe_test_normalized = preprocess_dataframes(save=True)

    print('Main ran')

    dataframe_normalized = pd.read_csv(
        '~/machine_learning/affiliate-desirability/experimentation/Main_Normalized_Undesirability_Dataset_2018-05-08.csv')
    dataframe_test_normalized = pd.read_csv(
        '~/machine_learning/affiliate-desirability/experimentation/Test_Normalized_Undesirability_Dataset_2018-05-08.csv')

    # Labels train
    labels_train = dataframe_normalized['Undesirable'].values
    labels_test = dataframe_test_normalized['Undesirable'].values

    # Balance dataset?
    # dataframe_normalized, labels_train = downsample_balance_dataset(dataframe_normalized)
    # dataframe_normalized, labels_train = upsample_balance_dataset(dataframe_normalized)

    # independent variable = Gross_Margin_126
    features = ["rev_VS_cost_36", "total_cancel_rate_36", "avg_net_rev_36", "Net_Settlement_36_Perc", "Gross_Margin_36",
                "GM_growth_rate_36", "rev_VS_cost_36"]

    training_set = dataframe_normalized[list(features)].values
    testing_set = dataframe_test_normalized[list(features)].values

    model = train_model(training_set, labels_train, len(features))
    predictions = create_predictions(model, testing_set)

    print(predictions)

    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(predictions, labels_test,
                                                                                   prediction_threshold)

    _, _, _, roc_auc = area_under_curve(predictions, labels_test, prediction_threshold)

    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['model_name'] = str(type(model))
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['features_used'] = features
    results_dictionary['hyperparameters'] = {'seed': seed}
    results_dictionary['precision-specificity(tp/tp+fp)'] = precision
    results_dictionary['recall-sensitivity(tp/tp+fn)'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_desc': ['[TN, FP]', '[FN, TP]'],
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold
    results_dictionary['area_under_curve'] = roc_auc

    pp(results_dictionary)

    # create_results_file(file_name, results_dictionary)
    # save_model(model, 'model_'+str(type(model)))


if __name__ == '__main__':
    main()
