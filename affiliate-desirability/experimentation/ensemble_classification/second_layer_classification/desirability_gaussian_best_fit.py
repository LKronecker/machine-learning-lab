import pandas as pd
from matplotlib import pyplot as plt
import numpy as np
from sklearn.mixture import GaussianMixture

# from sklearn.utils import

# from astroML.plotting import setup_text_plots
# setup_text_plots(fontsize=8, usetex=True)

features = ['Gross_Margin_126']


def main():
    train_dataframe = pd.read_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
                                  'ensemble_classification/second_layer_classification/'
                                  'Multiclass_Undesirability_Train_Dataset_05_16_2018.csv')


    train_set = train_dataframe[list(features)].values
    labels_train = train_dataframe['Undesirability_class']

    dependent_var = train_dataframe['Gross_Margin_126']


    N = np.arange(1, 11)
    models = [None for i in range(len(N))]

    for i in range(len(N)):
        models[i] = GaussianMixture(N[i]).fit(train_set)

    # compute the AIC and the BIC
    AIC = [m.aic(train_set) for m in models]
    BIC = [m.bic(train_set) for m in models]


    # Plot

    fig = plt.figure(figsize=(5, 1.7))
    fig.subplots_adjust(left=0.12, right=0.97,
                        bottom=0.21, top=0.9, wspace=0.5)

    # plot 1: data + best-fit mixture

    ax = fig.add_subplot(131)
    M_best = models[np.argmin(AIC)]

    x = np.linspace(-6, 6, 1000)
    logprob = M_best.score_samples(x)
    responsibilities = M_best._estimate_log_prob(x)
    pdf = np.exp(logprob)
    pdf_individual = responsibilities * pdf[:, np.newaxis]

    ax.hist(train_set, 30, normed=True, histtype='stepfilled', alpha=0.4)
    ax.plot(x, pdf, '-k')
    ax.plot(x, pdf_individual, '--k')
    ax.text(0.04, 0.96, "Best-fit Mixture",
            ha='left', va='top', transform=ax.transAxes)
    ax.set_xlabel('$x$')
    ax.set_ylabel('$p(x)$')

    # plot 2: AIC and BIC

    # ax = fig.add_subplot(132)
    # ax.plot(N, AIC, '-k', label='AIC')
    # ax.plot(N, BIC, '--k', label='BIC')
    # ax.set_xlabel('n. components')
    # ax.set_ylabel('information criterion')
    # ax.legend(loc=2)

    plt.show()

if __name__ == '__main__':
    main()