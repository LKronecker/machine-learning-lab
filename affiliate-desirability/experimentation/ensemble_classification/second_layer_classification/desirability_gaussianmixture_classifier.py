import pandas as pd
import numpy as np
from sklearn import mixture
from sklearn.metrics import classification_report, accuracy_score
# Plotting
import matplotlib.pyplot as plt
import matplotlib as mpl
import itertools
from scipy import linalg
#####
import scipy.stats as stats
import matplotlib.pyplot as plt
import pylab as pl


# features = ['Gross_Margin_36', 'GM_growth_rate_36', 'rev_VS_cost_36', 'avg_net_rev_36', 'Gross_Margin_6',
#             'SPU_sales_ratio', 'avg_rev_36', 'total_cancel_rate_36']

features = ['Gross_Margin_126'] # , 'affiliate_id'


def train_model(X):
    gaussian = mixture.GaussianMixture(n_components=2, covariance_type='full')
    gaussian.fit(X)
    return gaussian


def create_predictions(model, test_set):
    predictions = model.predict(test_set)
    return predictions


def create_probability_distributions(model, test_set):
    probabilities = model.predict_proba(test_set)
    return probabilities


def plot_results(X, Y_, means, covariances, index, title):
    color_iter = itertools.cycle(['navy', 'c', 'cornflowerblue', 'gold', 'darkorange'])
    splot = plt.subplot(2, 1, 1 + index)
    for i, (mean, covar, color) in enumerate(zip(
            means, covariances, color_iter)):
        v, w = linalg.eigh(covar)
        v = 2. * np.sqrt(2.) * np.sqrt(v)
        u = w[0] / linalg.norm(w[0])
        # as the DP will not use every component it has access to
        # unless it needs it, we shouldn't plot the redundant
        # components.
        if not np.any(Y_ == i):
            continue
        plt.scatter(X[Y_ == i, 0], X[Y_ == i, 1], .8, color=color)

        # Plot an ellipse to show the Gaussian component
        angle = np.arctan(u[1] / u[0])
        angle = 180. * angle / np.pi  # convert to degrees
        ell = mpl.patches.Ellipse(mean, v[0], v[1], 180. + angle, color=color)
        ell.set_clip_box(splot.bbox)
        ell.set_alpha(0.5)
        splot.add_artist(ell)

    plt.xlim(-9., 5.)
    plt.ylim(-3., 6.)
    plt.xticks(())
    plt.yticks(())
    plt.title(title)

    plt.show()


def plot_normal_distribution(feature):
    feature.sort()
    hmean = np.mean(feature)
    hstd = np.std(feature)
    pdf = stats.norm.pdf(feature, hmean, hstd)
    plt.plot(feature, pdf)
    plt.show()


def plot_normal_distribution_v2(feature):
    fit = stats.norm.pdf(feature, np.mean(feature), np.std(feature))  # this is a fitting indeed
    pl.plot(feature, fit, '-o')
    pl.hist(feature, normed=True)  # use this to draw histogram of your data
    pl.show()


def main():
    train_dataframe = pd.read_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
                                  'ensemble_classification/second_layer_classification/'
                                  'Multiclass_Undesirability_Train_Dataset_05_16_2018.csv')

    test_dataframe = pd.read_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
                                 'ensemble_classification/second_layer_classification/'
                                 'Multiclass_Undesirability_Test_Dataset_05_16_2018.csv')

    train_set = train_dataframe[list(features)].values
    labels_train = train_dataframe['Undesirability_class']

    test_set = test_dataframe[list(features)].values
    labels_test = test_dataframe['Undesirability_class']

    #Plot independent Normal Distribution
    plot_normal_distribution_v2(list(train_dataframe['Gross_Margin_126']))

    # gmm = train_model(train_set, labels_train)

    # Fit a Gaussian mixture with EM using five components
    gmm = mixture.GaussianMixture(n_components=3, covariance_type='full').fit(train_set)
    plot_results(train_set, gmm.predict(train_set), gmm.means_, gmm.covariances_, 0,
                 'Gaussian Mixture')

    # Fit a Dirichlet process Gaussian mixture using five components
    # dpgmm = mixture.BayesianGaussianMixture(n_components=5,
    #                                         covariance_type='full').fit(train_set)
    # plot_results(train_set, dpgmm.predict(train_set), dpgmm.means_, dpgmm.covariances_, 1,
    #              'Bayesian Gaussian Mixture with a Dirichlet process prior')

    # probability_dist = create_probability_distributions(gmm, test_set)
    # predictions = create_predictions(gmm, test_set)

    # pp(probability_dist)
    print('///////////////////////////////')

    result_dataframe = test_dataframe
    # result_dataframe['Predictions'] = predictions.tolist()

    # Print metrics
    # print('\nGaussian_Classifier - Testing Confusion Matrix\n\n', pd.crosstab(labels_test, predictions,
    #                                                                      rownames=['Actuall'], colnames=['Predicted']))
    #
    # print('\nGaussian_Classifier - Clasification report\n', classification_report(labels_test, predictions))
    #
    # print('\nGaussian_Classifier - Test Accuracy\n', round(accuracy_score(labels_test, predictions), 3))


if __name__ == '__main__':
    main()