import pandas as pd
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import numpy as np
from pprint import pprint as pp

# feed in al features to start
features = ['PU', 'SPU', 'PU_conversion_rate', 'Sales', 'sales_PU_ratio', 'SPU_sales_ratio', 'Total_Cost',
            'avg_cost_sale', 'avg_rev_6', 'avg_rev_36', 'rev_growth_rate_36','avg_net_rev_6', 'avg_net_rev_36',
            'rev_VS_cost_6', 'rev_VS_cost_36', 'Aff_size', 'Mem_cancel_6', 'Mem_cancel_36', 'cancel_rate_1',
            'cancel_rate_6', 'cancel_rate_36', 'total_cancel_rate_6','total_cancel_rate_36', 'US_mmb_rate',
            'GB_mmb_rate', 'MX_mmb_rate', 'IT_mmb_rate', 'CA_mmb_rate', 'BR_mmb_rate', 'avg_US_CPS', 'avg_GB_CPS',
            'avg_MX_CPS', 'avg_IT_CPS', 'avg_CA_CPS', 'avg_BR_CPS',
            'Gross_Setttlm_6', 'Gross_Setttlm_36', 'Gross_Setttlm_total_6',
            'Gross_Setttlm_total_36', 'Net_Setttlm_6', 'Net_Setttlm_36', 'Net_Setttlm_total_6',
            'Net_Setttlm_total_36', 'Net_Settlement_6_Perc', 'Net_Settlement_36_Perc', 'Processing_Cost_6',
            'Processing_Cost_36','Processing_Cost_6_Perc', 'Processing_Cost_36_Perc', 'Refunds_6',
            'Refunds_36', 'Refunds_Total_6', 'Refunds_Total_36', 'Disputes_6', 'Disputes_36', 'Disputes_total_6',
            'Disputes_total_36', 'Gross_Margin_6', 'Gross_Margin_36', 'GM_growth_rate_36']


def train_model(X):
    kmeans = KMeans(n_clusters=3, random_state=0)
    kmeans.fit(X)
    print('Labels')
    print(kmeans.labels_)
    return kmeans


def create_predictions(model, test_set):
    predictions = model.predict(test_set)
    return predictions


# Principal Component Analysis for dimensionality reduction
def apply_PCA(X):
    pca = PCA(n_components=3)
    pca.fit(X)
    transformed_set = pca.transform(X)
    return transformed_set


def main():
    train_dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                                 'ensemble_classification/second_layer_classification/'
                                 'Multiclass_Normalized_Train_Undesirability_Dataset_2018-05-17.csv')

    train_set = train_dataframe[list(features)].values
    labels_train = train_dataframe['Undesirability_class']

    test_dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                                 'ensemble_classification/second_layer_classification/'
                                 'Multiclass_Normalized_Test_Undesirability_Dataset_2018-05-17.csv')

    test_set = test_dataframe[list(features)].values
    labels_test = test_dataframe['Undesirability_class']

    # Apply PCA  #
    train_set = apply_PCA(train_set)
    test_set = apply_PCA(test_set)

    kmeans = train_model(train_set)

    predictions = create_predictions(kmeans, test_set)

    print('///////////////////////////////')
    pp(predictions)

    result_dataframe = test_dataframe
    result_dataframe['Results'] = predictions.tolist()



if __name__ == '__main__':
    main()