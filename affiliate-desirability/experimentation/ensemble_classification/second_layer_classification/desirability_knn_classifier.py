import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
from sklearn.neighbors import KNeighborsClassifier
from pprint import pprint as pp
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.metrics import classification_report, accuracy_score
from imblearn.over_sampling import SMOTE

use_normalized_data = False
use_pca = False
use_resampling = False
use_cv_folds = False


# features = ['Gross_Margin_36', 'GM_growth_rate_36', 'rev_VS_cost_36', 'avg_net_rev_36', 'Gross_Margin_6',
#             'SPU_sales_ratio', 'avg_rev_36', 'total_cancel_rate_36', 'avg_net_rev_6', 'PU_conversion_rate',
#             'rev_VS_cost_6']

features = ['Gross_Margin_36', 'GM_growth_rate_36', 'rev_VS_cost_36', 'avg_net_rev_36', 'Gross_Margin_6',
            'SPU_sales_ratio', 'avg_rev_36', 'total_cancel_rate_36']


def crossvalidation_scores(X_train, y_train):
    # creating odd list of K for KNN
    myList = list(range(1, 50))

    # subsetting just the odd ones
    neighbors = list(filter(lambda x: x % 2 != 0, myList))

    # empty list that will hold cv scores
    cv_scores = []

    # perform 10-fold cross validation
    for k in neighbors:
        knn = KNeighborsClassifier(n_neighbors=k)
        scores = cross_val_score(knn, X_train, y_train, cv=10, scoring='accuracy')
        cv_scores.append(scores.mean())

    # changing to misclassification error
    MSE = [1 - x for x in cv_scores]

    # determining best k
    optimal_k = neighbors[MSE.index(min(MSE))]
    print("The optimal number of neighbors is")
    print(optimal_k)

    # plot misclassification error vs k
    plt.plot(neighbors, MSE)
    plt.xlabel('Number of Neighbors K')
    plt.ylabel('Misclassification Error')
    plt.show()

    return optimal_k, cv_scores


# Kmeans model
def train_model(X, y, n_neighbors):
    neigh = KNeighborsClassifier(n_neighbors=n_neighbors)
    neigh.fit(X, y)
    return neigh


def create_predictions(model, test_set):
    predictions = model.predict(test_set)
    return predictions


def create_probability_distributions(model, test_set):
    probabilities = model.predict_proba(test_set)
    return probabilities

# Principal Component Analysis for dimensionality reduction
def apply_PCA(X):
    pca = PCA(n_components=3)
    pca.fit(X)
    transformed_set = pca.transform(X)
    return transformed_set


def plot_boundaries(train_dataframe, n_neighbors, feature_names):

    X = train_dataframe[list(feature_names)].values
    y = train_dataframe['Undesirability_class']

    h = .02  # step size in the mesh

    # Create color maps
    cmap_light = ListedColormap(['#FFAAAA', '#AAFFAA', '#AAAAFF'])
    cmap_bold = ListedColormap(['#FF0000', '#00FF00', '#0000FF'])

    for weights in ['uniform', 'distance']:
        # we create an instance of Neighbours Classifier and fit the data.
        clf = KNeighborsClassifier(n_neighbors, weights=weights)
        clf.fit(X, y)

        # Plot the decision boundary. For that, we will assign a color to each
        # point in the mesh [x_min, x_max]x[y_min, y_max].
        x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
        y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                             np.arange(y_min, y_max, h))
        Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])

        # Put the result into a color plot
        Z = Z.reshape(xx.shape)
        plt.figure()
        plt.pcolormesh(xx, yy, Z, cmap=cmap_light)

        # Plot also the training points
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=cmap_bold,
                    edgecolor='k', s=20)
        plt.xlim(xx.min(), xx.max())
        plt.ylim(yy.min(), yy.max())
        plt.title("3-Class classification (k = %i, weights = '%s')"
                  % (n_neighbors, weights))
        plt.xlabel(feature_names[0])
        plt.ylabel(feature_names[1])

    plt.show()


def main():

    if use_normalized_data == True:
        train_dataframe = pd.read_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
                                      'ensemble_classification/second_layer_classification/'
                                      'Multiclass_Normalized_Train_Undesirability_Dataset_2018-05-17.csv')

        test_dataframe = pd.read_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
                                     'ensemble_classification/second_layer_classification/'
                                     'Multiclass_Normalized_Test_Undesirability_Dataset_2018-05-17.csv')


    else :
        train_dataframe = pd.read_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
                                      'ensemble_classification/second_layer_classification/'
                                      'Multiclass_Undesirability_Train_Dataset_05_16_2018.csv')

        test_dataframe = pd.read_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
                                     'ensemble_classification/second_layer_classification/'
                                     'Multiclass_Undesirability_Test_Dataset_05_16_2018.csv')



    # standarize each of the features to have mean zero and variance 1 (Normalize features)
    train_set = train_dataframe[list(features)].values
    labels_train = train_dataframe['Undesirability_class']

    test_set = test_dataframe[list(features)].values
    labels_test = test_dataframe['Undesirability_class']

    # Resampling #
    if use_resampling == True:
        sm = SMOTE(ratio='minority', kind='regular')
        train_set, labels_train = sm.fit_sample(train_set, labels_train)

    # Apply PCA #
    if use_pca == True:
        train_set = apply_PCA(train_set)
        test_set = apply_PCA(test_set)

    if use_cv_folds == True:
        optimal_k, _ = crossvalidation_scores(train_set, labels_train)
    else:
        # define number of neighbours manually (10 is the best value up to now)
        optimal_k = 11

    neigh = train_model(train_set, labels_train, optimal_k)
    # plot_boundaries(train_set, labels_train, optimal_k, features)
    choose_best_features(train_dataframe, optimal_k, features)

    # probability_dist = create_probability_distributions(neigh, test_set)
    predictions = create_predictions(neigh, test_set)

    # pp(probability_dist)
    print('///////////////////////////////')
    pp(predictions)

    result_dataframe = test_dataframe
    result_dataframe['Predictions'] = predictions.tolist()


    # Print metrics
    print('\nKNN_Classifier - Testing Confusion Matrix\n\n', pd.crosstab(labels_test, predictions,
                                                                   rownames=['Actuall'], colnames=['Predicted']))

    print('\nKNN_Classifier - Clasification report\n', classification_report(labels_test, predictions))

    print('\nKNN_Classifier - Test Accuracy\n', round(accuracy_score(labels_test, predictions), 3))


def choose_best_features(train_dataframe, n_neighbors, all_features):

    new_all_features = all_features[:]

    for feature in all_features:
        features_to_use = []
        features_to_use.append(feature)
        # print(features_to_use)
        new_all_features.remove(feature)
        # print(all_features)

        for feat in new_all_features:
            features_to_use.append(feat)
            print(features_to_use)
            #graph pair of features
            plot_boundaries(train_dataframe, n_neighbors, features_to_use)
            features_to_use.remove(feat)



if __name__ == '__main__':
    main()