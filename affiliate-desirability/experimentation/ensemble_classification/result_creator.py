import json
import pickle
import datetime
# import dill as pkle

# Date formatting
now = datetime.datetime.now()

def create_results_file(file_name, results_dictionary):
    # Uncomment to write result json file
    with open(file_name, 'w') as outfile:
        json.dump(results_dictionary, outfile)

    # Probability distribution per class. Uncomment bellow to create a csv file with results DF.
    # result_df = pd.DataFrame()
    # result_df['member_id'] = test_dataFrame['member_id']
    # result_df['Super_User'] = result_refined
    # result_df.to_csv('~/Prototypes/machine-learning-lab/su_classifier_results.csv', index=None)


## DEPRECATED. Use new format
def save_model(model, model_name):
    filename = model_name + str(datetime.datetime.now()) + '.sav'
    pickle.dump(model, open(filename, 'wb'))


## DEPRECATED. Use new format
def load_model(filename):
    loaded_model = pickle.load(open(filename, 'rb'))
    # result = loaded_model.score(X_test, Y_test)
    # print(result)
    return loaded_model


# def load_model_new(filename):
#     with open(filename, 'rb') as f:
#         loaded_model = pkle.load(f)
#         return loaded_model
#
#
# def save_model_new(model, model_name):
#     filename = model_name + str(datetime.datetime.now())
#     with open(filename, 'wb') as f:
#         pkle.dump(model, f, protocol=2)


