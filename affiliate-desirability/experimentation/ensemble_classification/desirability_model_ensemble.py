import pandas as pd
import desirability_extraTrees as xt
import desirability_boosting as bt
import desirability_randomforest as rf
import numpy as np
from result_creator import *
from metrics import *
from sklearn.metrics import classification_report, accuracy_score

execute_ensemble_six_day_time_period = True


def execute_model_36_days_ensemble(dataframe_test, save_results=False):
    labels_test = dataframe_test['Undesirable'].values

    # Extratrees
    extratrees = load_model('models/model_ExtraTreesClassifier_2018-05-11.sav')
    xt_testing_set = dataframe_test[list(xt.features)].values
    xt_predictions = xt.create_predictions(extratrees, xt_testing_set)
    xt_probabilities = xt.create_probability_distributions(extratrees, xt_testing_set)[:, 1]
    xt.evaluate_model_performance(extratrees, xt_predictions, labels_test, save_results_file=save_results)

    # AdaBoost
    adaboost = load_model('models/model_AdaBoostClassifier_2018-05-15.sav')
    bt_testing_set = dataframe_test[list(bt.features)].values
    bt_predictions = bt.create_predictions(adaboost, bt_testing_set)
    bt_probabilities = bt.create_probability_distributions(adaboost, bt_testing_set)[:, 1]
    bt.evaluate_model_performance(adaboost, bt_predictions, labels_test, save_results_file=save_results)

    # Randomforests
    randomforests = load_model('models/model_RandomForestClassifier_2018-05-11.sav')
    rf_testing_set = dataframe_test[list(rf.features)].values
    rf_predictions = rf.create_predictions(randomforests, rf_testing_set)
    rf_probabilities = rf.create_probability_distributions(randomforests, rf_testing_set)[:, 1]
    rf.evaluate_model_performance(randomforests, rf_predictions, labels_test, save_results_file=save_results)

    # Create combined results
    combined_results = []
    for i, val in enumerate(xt_predictions):
        combination = 0
        addedup = val + bt_predictions[i] + rf_predictions[i]
        if addedup > 1:
            combination = 1
        else:
            combination = 0

        combined_results.append(combination)

    return xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
           combined_results


def execute_model_6_days_ensemble(dataframe_test, save_results=False):
    labels_test = dataframe_test['Undesirable'].values

    # Extratrees
    extratrees = load_model('models/model_6_days_ExtraTreesClassifier_2018-05-11.sav')
    xt_testing_set = dataframe_test[list(xt.features)].values
    xt_predictions = xt.create_predictions(extratrees, xt_testing_set)
    xt_probabilities = xt.create_probability_distributions(extratrees, xt_testing_set)[:, 1]
    xt.evaluate_model_performance(extratrees, xt_predictions, labels_test, save_results_file=save_results)

    # AdaBoost
    adaboost = load_model('models/model_6_days_AdaBoostClassifier_2018-05-11.sav')
    bt_testing_set = dataframe_test[list(bt.features)].values
    bt_predictions = bt.create_predictions(adaboost, bt_testing_set)
    bt_probabilities = bt.create_probability_distributions(adaboost, bt_testing_set)[:, 1]
    bt.evaluate_model_performance(adaboost, bt_predictions, labels_test, save_results_file=save_results)

    # Randomforests
    randomforests = load_model('models/model_6_days_RandomForestClassifier_2018-05-11.sav')
    rf_testing_set = dataframe_test[list(rf.features)].values
    rf_predictions = rf.create_predictions(randomforests, rf_testing_set)
    rf_probabilities = rf.create_probability_distributions(randomforests, rf_testing_set)[:, 1]
    rf.evaluate_model_performance(randomforests, rf_predictions, labels_test, save_results_file=save_results)

    # Create combined results
    combined_results = []
    for i, val in enumerate(xt_predictions):
        combination = 0
        addedup = val + bt_predictions[i] + rf_predictions[i]
        if addedup > 1:
            combination = 1
        else:
            combination = 0

        combined_results.append(combination)

    return xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
           combined_results

def main():

    # Original testing set
    dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/original-datasets/'
                                 'Final_Undesirability_Test_Dataset_05_10_2018.csv')

    # New testing set (Different_CPAs)
    # dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/original-datasets/'
    #                              'Desirability_Different_CPAs.csv')

    labels_test = dataframe_test['Undesirable'].values

    dataframe_test.drop(['Desirability_Score'], axis=1, inplace=True)

    results_dataframe = dataframe_test

    if execute_ensemble_six_day_time_period == True:
        ### Load models for 6 days ###
        xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
        combined_results = execute_model_6_days_ensemble(dataframe_test, save_results=False)

        results_dataframe['Combined_6'] = combined_results

        results_dataframe['ExtraTrees_6'] = xt_predictions
        results_dataframe['ExtraTrees_Prob_6'] = xt_probabilities

        results_dataframe['Adaboost_6'] = bt_predictions
        results_dataframe['Adaboost_Prob_6'] = bt_probabilities

        results_dataframe['Randomforests_6'] = rf_predictions
        results_dataframe['Randomforests_Prob_6'] = rf_probabilities

        # results_dataframe.to_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
        #                          'ensemble_classification/results/Different_CPAs/6_days/6_days_results_table.csv',
        #                          index=None)

    else:
        ### Load models for 36 days ###
        xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
        combined_results = execute_model_36_days_ensemble(dataframe_test, save_results=False)

        results_dataframe['Combined_36'] = combined_results

        results_dataframe['ExtraTrees_36'] = xt_predictions
        results_dataframe['ExtraTrees_Prob_36'] = xt_probabilities

        results_dataframe['Adaboost_36'] = bt_predictions
        results_dataframe['Adaboost_Prob_36'] = bt_probabilities

        results_dataframe['Randomforests_36'] = rf_predictions
        results_dataframe['Randomforests_Prob_36'] = rf_probabilities

        # results_dataframe.to_csv('/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/'
        #                          'ensemble_classification/results/Different_CPAs/36_days/36_days_results_table.csv',
        #                          index=None)

    # Print metrics
    print('\nCombination_Classification_Results - Testing Confusion Matrix\n\n',
          pd.crosstab(labels_test, np.array(combined_results), rownames=['Actuall'], colnames=['Predicted']))

    print('\nCombination_Classification_Results - Clasification report\n',
          classification_report(labels_test, np.array(combined_results)))

    print('\nCombination_Classification_Results - Test Accuracy\n',
          round(accuracy_score(labels_test, np.array(combined_results)), 3))

    # Apply desired metrics for model optimization
    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(np.array(combined_results),
                                                                                   labels_test)
    roc_auc = area_under_curve(np.array(combined_results), labels_test)

    # Create file for model documentation
    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['models'] = ['ExtraTrees', 'Adaboost', 'Randomforests']
    results_dictionary['six_day_time_period'] = execute_ensemble_six_day_time_period
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['precision-specificity(tp/tp+fp)'] = precision
    results_dictionary['recall-sensitivity(tp/tp+fn)'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_desc': ['[TN, FP]', '[FN, TP]'],
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][
                                                  1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['area_under_curve'] = roc_auc

    print(results_dictionary)
    # create_results_file(file_name, results_dictionary)


def create_combined_results_table(dataframe_test):

    six_days_results_dataframe = pd.read_csv(
        '/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/ensemble_classification/results/'
        'Different_CPAs/6_days/6_days_results_table.csv')

    thirtysix_days_results_dataframe = pd.read_csv(
        '/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/ensemble_classification/results/'
        'Different_CPAs/36_days/36_days_results_table.csv')

    combined_results_dataframe = dataframe_test
    combined_results_dataframe['Combined_6'] = six_days_results_dataframe['Combined_6']
    combined_results_dataframe['Combined_36'] = thirtysix_days_results_dataframe['Combined_36']

    combined_results_dataframe['ExtraTrees_6'] = six_days_results_dataframe['ExtraTrees_6']
    combined_results_dataframe['ExtraTrees_Prob_6'] = six_days_results_dataframe['ExtraTrees_Prob_6']
    combined_results_dataframe['ExtraTrees_36'] = thirtysix_days_results_dataframe['ExtraTrees_36']
    combined_results_dataframe['ExtraTrees_Prob_36'] = thirtysix_days_results_dataframe['ExtraTrees_Prob_36']

    combined_results_dataframe['Adaboost_6'] = six_days_results_dataframe['Adaboost_6']
    combined_results_dataframe['Adaboost_Prob_6'] = six_days_results_dataframe['Adaboost_Prob_6']
    combined_results_dataframe['Adaboost_36'] = thirtysix_days_results_dataframe['Adaboost_36']
    combined_results_dataframe['Adaboost_Prob_36'] = thirtysix_days_results_dataframe['Adaboost_Prob_36']

    combined_results_dataframe['Randomforests_6'] = six_days_results_dataframe['Randomforests_6']
    combined_results_dataframe['Randomforests_Prob_6'] = six_days_results_dataframe['Randomforests_Prob_6']
    combined_results_dataframe['Randomforests_36'] = thirtysix_days_results_dataframe['Randomforests_36']
    combined_results_dataframe['Randomforests_Prob_36'] = thirtysix_days_results_dataframe['Randomforests_Prob_36']

    combined_results_dataframe.to_csv(
        '/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/ensemble_classification/results/'
        'Different_CPAs/combined_results_table.csv', index=None)

    # both_results_dataframe = pd.read_csv(
    #     '/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/ensemble_classification/results/'
    #     'combined_results_table.csv')


if __name__ == '__main__':
    main()
