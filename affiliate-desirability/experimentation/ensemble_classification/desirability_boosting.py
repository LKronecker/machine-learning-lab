#Preprocessing
from desirability_preprocessing import *

#model
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier

# Metrics
from metrics import *

from pprint import pprint as pp
import datetime

# Results
from result_creator import *
# from helpers import *

import matplotlib.pyplot as plt
import numpy as np

# from imblearn.over_sampling import SMOTE

prediction_threshold = .5
seed = 260
np.random.seed(seed)

normalized_data = False
short_time_period = False

# Model HP
max_depth = 2 # Using 1 makes number fals positives frops more
criterion = 'gini'
algorithm = "SAMME"
n_estimators = 5000
learning_rate = 0.05

if short_time_period == True:
    features = ['Net_Setttlm_total_6', 'Gross_Margin_6', 'BR_mmb_rate', 'cancel_rate_1', 'Gross_Setttlm_6',
                'SPU_sales_ratio', 'PU_conversion_rate', 'Total_Cost', 'Processing_Cost_6', 'avg_net_rev_6',
                'sales_PU_ratio']

else:
    features = ['Gross_Margin_36', 'total_cancel_rate_36', 'cancel_rate_36', 'Total_Cost', 'cancel_rate_1',
                'Net_Setttlm_36', 'CA_mmb_rate', 'rev_growth_rate_36', 'rev_VS_cost_36', 'GM_growth_rate_36',
                'avg_net_rev_36']



def train_model(features_train, labels_train):
    dTree = DecisionTreeClassifier(max_depth=max_depth, criterion=criterion)

    adaboost = AdaBoostClassifier(base_estimator=dTree, algorithm=algorithm, n_estimators=n_estimators,
                                  learning_rate=learning_rate, random_state=seed)

    adaboost.fit(features_train, labels_train)
    pp(adaboost)

    # plot_boundaries(features_train, labels_train, adaboost)

    return adaboost


def create_predictions(model, features_test):
    probabilities = create_probability_distributions(model, features_test)
    predictions_filtered = np.array([1 if probabilities[:, 1][k] > prediction_threshold else 0 for k in
                                     range(len(probabilities[:, 1]))])
    return predictions_filtered

def create_probability_distributions(model, features_test):
    probabilities = model.predict_proba(features_test)
    return probabilities


def evaluate_model_performance(model, predictions, labels_test, save_results_file=False):
    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(predictions, labels_test)
    roc_auc = area_under_curve(predictions, labels_test)

    # Print metrics
    print('\nAdaboost - Testing Confusion Matrix\n\n', pd.crosstab(labels_test, predictions,
                                                                   rownames=['Actuall'], colnames=['Predicted']))

    print('\nAdaboost - Clasification report\n', classification_report(labels_test, predictions))

    print('\nAdaboost - Test Accuracy\n', round(accuracy_score(labels_test, predictions), 3))


    # Create file for model documentation
    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['normalized_data'] = normalized_data
    results_dictionary['short_time_period'] = short_time_period
    results_dictionary['model_name'] = str(type(model))
    results_dictionary['normalized_data'] = normalized_data
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['features_used'] = features
    results_dictionary['hyperparameters'] = {
        'base_estimator': {'name': 'DecisionTreeClassifier',
                           'max_depth': max_depth},
        'algorithm': algorithm,
        'learning_rate': 0.05,
        'n_estimators': n_estimators,
        'random_state': seed}

    results_dictionary['precision-specificity(tp/tp+fp)'] = precision
    results_dictionary['recall-sensitivity(tp/tp+fn)'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_desc': ['[TN, FP]', '[FN, TP]'],
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold
    results_dictionary['area_under_curve'] = roc_auc

    pp(results_dictionary)

    if save_results_file == True:
        create_results_file(file_name, results_dictionary)


def main():
    if normalized_data == True:
        dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/normalized-datasets/'
                                'Final_Normalized_Train_Undesirability_Dataset_2018-05-11.csv')
        dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/normalized-datasets/'
                                        'Final_Normalized_Test_Undesirability_Dataset_2018-05-11.csv')

    else:
        dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/original-datasets/'
                                'Final_Undesirability_Train_Dataset_05_10_2018.csv')

        dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/original-datasets/'
                                     'Final_Undesirability_Test_Dataset_05_10_2018.csv')

    # Labels train
    labels_train = dataframe['Undesirable'].values
    labels_test = dataframe_test['Undesirable'].values

    # Balance dataset
    # dataframe, labels_train = downsample_balance_dataset(dataframe)
    # dataframe, labels_train = upsample_balance_dataset(dataframe)


    training_set = dataframe[list(features)].values
    testing_set = dataframe_test[list(features)].values

    ######## Resampling ########
    # sm = SMOTE(ratio='minority', kind='regular', random_state=seed)
    # X_res, y_res = sm.fit_sample(training_set, labels_train)
    ###########################

    adaboost = train_model(training_set, labels_train)
    # plot_feature_ranking(adaboost, training_set, features)

    predictions = create_predictions(adaboost, testing_set)
    evaluate_model_performance(adaboost, predictions, labels_test, save_results_file=True)

    # save_model(adaboost, 'model_' + str(type(adaboost)))


if __name__ == '__main__':
    main()