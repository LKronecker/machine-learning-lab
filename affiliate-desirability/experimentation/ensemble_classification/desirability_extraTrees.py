#Preprocessing
from desirability_preprocessing import *

# Metrics
from metrics import *

# from helpers import plot_feature_ranking

#model
from sklearn.ensemble import ExtraTreesClassifier

# Results
from result_creator import *

#helpers
from pprint import pprint as pp
import numpy as np
import datetime

prediction_threshold = .5 #.72
seed = 132069
np.random.seed(seed)
short_time_period = False
normalized_data = False

# Model Hyper parameters. Optimize here if desired
n_estimators = 50
max_depth = None
max_features = 'sqrt'
random_state = seed
criterion = 'entropy' # ginni
bootstrap = True

if short_time_period == True:
    features = ['Gross_Margin_6', 'avg_net_rev_6', 'SPU_sales_ratio', 'rev_VS_cost_6', 'Net_Settlement_6_Perc',
                'avg_rev_6', 'PU_conversion_rate', 'total_cancel_rate_6', 'Processing_Cost_6_Perc', 'US_mmb_rate',
                'cancel_rate_6']

else:

    features = ['Gross_Margin_36', 'rev_VS_cost_36', 'avg_net_rev_36', 'Gross_Margin_6', 'rev_VS_cost_6',
                'total_cancel_rate_36', 'PU_conversion_rate', 'avg_rev_36', 'Net_Settlement_6_Perc',
                'Net_Settlement_36_Perc', 'SPU_sales_ratio']


def train_model(features_train, labels_train):
    extratrees = ExtraTreesClassifier(n_estimators=n_estimators, max_features=max_features, random_state=random_state,
                                      max_depth=max_depth, criterion=criterion, bootstrap=bootstrap)

    extratrees.fit(features_train, labels_train)
    return extratrees


def create_predictions(model, features_test):
    probabilities = create_probability_distributions(model, features_test)
    predictions_filtered = np.array([1 if probabilities[:, 1][k] > prediction_threshold else 0 for k in
                                     range(len(probabilities[:, 1]))])
    return predictions_filtered

def create_probability_distributions(model, features_test):
    probabilities = model.predict_proba(features_test)
    return probabilities


def evaluate_model_performance(model, predictions, labels_test, save_results_file=False):
    precision, recall, conf_mtrx, f1score, mats_coefficient = evaluate_predictions(predictions, labels_test)
    roc_auc = area_under_curve(predictions, labels_test)

    # Print metrics
    print('\nExtraTrees - Testing Confusion Matrix\n\n', pd.crosstab(labels_test, predictions,
                                                                     rownames=['Actuall'], colnames=['Predicted']))

    print('\nExtraTrees - Clasification report\n', classification_report(labels_test, predictions))

    print('\nExtraTrees - Test Accuracy\n', round(accuracy_score(labels_test, predictions), 3))

    file_name = 'Classification_Results_' + str(now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['model_name'] = str(type(model))
    results_dictionary['short_time_period'] = short_time_period
    results_dictionary['normalized_data'] = normalized_data
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['features_used'] = features
    results_dictionary['hyperparameters'] = {'seed': seed, 'n_estimators': n_estimators, 'max_depth': max_depth,
                                             'max_features': max_features, 'random_state': random_state,
                                             'criterion': criterion, 'bootstrap': bootstrap}

    results_dictionary['precision-specificity(tp/tp+fp)'] = precision
    results_dictionary['recall-sensitivity(tp/tp+fn)'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_desc': ['[TN, FP]', '[FN, TP]'],
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold
    results_dictionary['area_under_curve'] = roc_auc

    pp(results_dictionary)

    if save_results_file == True:
        create_results_file(file_name, results_dictionary)


def main():
    if normalized_data == True:
        dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/normalized-datasets/'
                                'Final_Normalized_Train_Undesirability_Dataset_2018-05-11.csv')
        dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/normalized-datasets/'
                                    'Final_Normalized_Test_Undesirability_Dataset_2018-05-11.csv')

    else:
        dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/original-datasets/'
                                'Final_Undesirability_Train_Dataset_05_10_2018.csv')

        dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/original-datasets/'
                                     'Final_Undesirability_Test_Dataset_05_10_2018.csv')

    # Labels train
    labels_train = dataframe['Undesirable'].values
    labels_test = dataframe_test['Undesirable'].values

    # Balance dataset?
    # dataframe_normalized, labels_train = downsample_balance_dataset(dataframe_normalized)
    # dataframe_normalized, labels_train = upsample_balance_dataset(dataframe_normalized)


    training_set = dataframe[list(features)].values
    testing_set = dataframe_test[list(features)].values

    model = train_model(training_set, labels_train)
    # plot_feature_ranking(model, training_set, features)

    predictions = create_predictions(model, testing_set)

    evaluate_model_performance(model, predictions, labels_test, save_results_file=True)
    # save_model(model, 'model_'+str(type(model)))


if __name__ == '__main__':
    main()
