import pandas as pd
import datetime
from sklearn.utils import shuffle

desirability_threshold = .25

# Usable features

# For 36 days cycle
# features = ['PU', 'SPU', 'PU_conversion_rate', 'Sales', 'sales_PU_ratio', 'SPU_sales_ratio', 'Total_Cost',
#             'avg_cost_sale', 'avg_rev_6', 'avg_rev_36', 'rev_growth_rate_36','avg_net_rev_6', 'avg_net_rev_36',
#             'rev_VS_cost_6', 'rev_VS_cost_36', 'Aff_size', 'Mem_cancel_6', 'Mem_cancel_36', 'cancel_rate_1',
#             'cancel_rate_6', 'cancel_rate_36', 'total_cancel_rate_6','total_cancel_rate_36', 'US_mmb_rate',
#             'GB_mmb_rate', 'MX_mmb_rate', 'IT_mmb_rate', 'CA_mmb_rate', 'BR_mmb_rate', 'avg_US_CPS', 'avg_GB_CPS',
#             'avg_MX_CPS', 'avg_IT_CPS', 'avg_CA_CPS', 'avg_BR_CPS',
#             'Gross_Setttlm_6', 'Gross_Setttlm_36', 'Gross_Setttlm_total_6',
#             'Gross_Setttlm_total_36', 'Net_Setttlm_6', 'Net_Setttlm_36', 'Net_Setttlm_total_6',
#             'Net_Setttlm_total_36', 'Net_Settlement_6_Perc', 'Net_Settlement_36_Perc', 'Processing_Cost_6',
#             'Processing_Cost_36','Processing_Cost_6_Perc', 'Processing_Cost_36_Perc', 'Refunds_6',
#             'Refunds_36', 'Refunds_Total_6', 'Refunds_Total_36', 'Disputes_6', 'Disputes_36', 'Disputes_total_6',
#             'Disputes_total_36', 'Gross_Margin_6', 'Gross_Margin_36', 'GM_growth_rate_36']

# For 6 days
# features = ['PU', 'SPU', 'PU_conversion_rate', 'Sales', 'sales_PU_ratio', 'SPU_sales_ratio', 'Total_Cost',
#             'avg_cost_sale', 'avg_rev_6', 'avg_net_rev_6', 'rev_VS_cost_6', 'Mem_cancel_6',
#             'cancel_rate_1', 'cancel_rate_6', 'total_cancel_rate_6', 'US_mmb_rate', 'GB_mmb_rate', 'MX_mmb_rate',
#             'IT_mmb_rate', 'CA_mmb_rate', 'BR_mmb_rate', 'avg_US_CPS', 'avg_GB_CPS', 'avg_MX_CPS', 'avg_IT_CPS',
#             'avg_CA_CPS', 'avg_BR_CPS', 'Gross_Setttlm_6', 'Gross_Setttlm_total_6', 'Net_Setttlm_6',
#             'Net_Setttlm_total_6', 'Net_Settlement_6_Perc', 'Processing_Cost_6', 'Processing_Cost_6_Perc', 'Refunds_6',
#             'Refunds_Total_6', 'Disputes_6', 'Disputes_total_6', 'Gross_Margin_6']


def preprocess_dataframes(save=False):

    dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                            'ensemble_classification/second_layer_classification/'
                            'Multiclass_Undesirability_Train_Dataset_05_16_2018.csv')
    # dataframe['Undesirable'] = [0 if dataframe['Gross_Margin_126'].values[k] > desirability_threshold else 1 for k in
    #                           range(len(dataframe['Desirability_Score'].values))]
    dataframe.to_csv(
        '~/machine_learning/affiliate-desirability/experimentation/original-datasets/Final_Undesirability_Train_Dataset_05_10_2018.csv',
        index=None)

    # dataframe.replace('Yes', 1, inplace=True)
    # dataframe.replace('No', 0, inplace=True)
    # dataframe.drop(['first_sale_date'], axis=1, inplace=True)

    dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                            'ensemble_classification/second_layer_classification/'
                            'Multiclass_Undesirability_Test_Dataset_05_16_2018.csv')

    # dataframe_test['Undesirable'] = [0 if dataframe_test['Desirable'].values[k] == 1 else 1 for k in
    #                             range(len(dataframe_test['Desirable'].values))]
    # dataframe_test.to_csv('~/machine_learning/affiliate-desirability/experimentation/original-datasets/Final_Undesirability_Test_Dataset_05_10_2018.csv', index=None)

    # dataframe_test.drop(['first_sale_date'], axis=1, inplace=True)

    df_copy = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                            'ensemble_classification/second_layer_classification/'
                            'Multiclass_Undesirability_Train_Dataset_05_16_2018.csv')
    df_copy.drop(['affiliate_id', 'affiliate_type', 'Desirability_Score', 'Undesirable', 'Undesirability_class'],
                 axis=1, inplace=True)

    normalization_coeffs = normalization_coefficients(df_copy, df_copy.columns)

    dataframe_normalized = normalize_values(dataframe, df_copy.columns, normalization_coeffs)
    dataframe_normalized = shuffle(dataframe_normalized)

    dataframe_test_normalized = normalize_values(dataframe_test, df_copy.columns, normalization_coeffs)
    dataframe_test_normalized = shuffle(dataframe_test_normalized)

    if save == True:
        dataframe_normalized_path = '~/machine_learning/affiliate-desirability/experimentation/' \
                                    'ensemble_classification/second_layer_classification/' \
                                    'Multiclass_Normalized_Train_Undesirability_Dataset_' + str(datetime.datetime.now()) + '.csv'

        dataframe_normalized.to_csv(dataframe_normalized_path, index=None)

        dataframe_test_normalized_path = '~/machine_learning/affiliate-desirability/experimentation/' \
                                    'ensemble_classification/second_layer_classification/' \
                                    'Multiclass_Normalized_Test_Undesirability_Dataset_' + str(datetime.datetime.now()) + '.csv'
        dataframe_test_normalized.to_csv(dataframe_test_normalized_path, index=None)

    return dataframe_normalized, dataframe_test_normalized


def normalization_coefficients(base_dataFrame, feature_list):
    max_list = []
    min_list = []
    for feature_name in feature_list:
        max_list.append(max(list(base_dataFrame[feature_name].values)))
        min_list.append(min(list(base_dataFrame[feature_name].values)))

    return list(zip(max_list, min_list))


def normalize_values(dataframe, feature_list, normalization_coeffs):
    # Create new dataframe & Save features to be left intact
    normalized_dataframe = pd.DataFrame()
    normalized_dataframe['affiliate_id'] = dataframe['affiliate_id']
    normalized_dataframe['affiliate_type'] = dataframe['affiliate_type']

    # Clean dataframe
    dataframe.drop(['affiliate_id', 'affiliate_type'], axis=1, inplace=True)

    for i, feature_name in enumerate(feature_list):
        values = list(dataframe[feature_name].values)
        max = normalization_coeffs[i][0]
        min = normalization_coeffs[i][1]

        for j, value in enumerate(values):
            normalized_value = (value - min) / (max - min)
            # clip values between 0 and 1
            if normalized_value < 0:
                normalized_value = 0
            elif normalized_value > 1:
                normalized_value = 1

            values[j] = normalized_value

        normalized_dataframe[feature_name] = values

    # Append these features to the end to keep the order of the DFs
    normalized_dataframe['Desirability_Score'] = dataframe['Desirability_Score']
    normalized_dataframe['Undesirable'] = dataframe['Undesirable']
    normalized_dataframe['Undesirability_class'] = dataframe['Undesirability_class']

    return normalized_dataframe


def downsample_balance_dataset(dataframe):
    # Shape (145, 72)
    undesirable_dataFrame = dataframe.loc[dataframe['Undesirable'] == 1]

    # Shape (696, 72)
    non_undesirable_dataFrame = dataframe.loc[dataframe['Undesirable'] == 0]
    #shape (145, 72)
    non_undesirable_dataFrame = non_undesirable_dataFrame[0:undesirable_dataFrame.shape[0]]

    balanced_dataFrame = pd.concat([undesirable_dataFrame, non_undesirable_dataFrame])

    balanced_dataFrame = shuffle(balanced_dataFrame)

    labels = balanced_dataFrame['Undesirable'].values

    return balanced_dataFrame, labels


def upsample_balance_dataset(dataframe):
    # Shape (145, 72)
    undesirable_dataframe = dataframe.loc[dataframe['Undesirable'] == 1]
    # Shape (696, 72)
    non_undesirable_dataframe = dataframe.loc[dataframe['Undesirable'] == 0]

    coeff = int(int(non_undesirable_dataframe.shape[0]) / int(undesirable_dataframe.shape[0]))
    coeff = int(coeff/2)

    augmented_undesirable_dataframe = pd.DataFrame()
    for i in range(coeff + 1):
        augmented_undesirable_dataframe = pd.concat([augmented_undesirable_dataframe, undesirable_dataframe])

    augmented_undesirable_dataframe = augmented_undesirable_dataframe[0:non_undesirable_dataframe.shape[0]]

    balanced_dataFrame = pd.concat([augmented_undesirable_dataframe, non_undesirable_dataframe])
    balanced_dataFrame = shuffle(balanced_dataFrame)

    labels = balanced_dataFrame['Undesirable'].values

    return balanced_dataFrame, labels


def create_train_set_for_second_layer_classification():
    # Train
    # original_dataframe = pd.read_csv(
    #     '~/machine_learning/affiliate-desirability/experimentation/original-datasets/Final_Undesirability_Train_Dataset_05_10_2018.csv')

    # Test
    original_dataframe = pd.read_csv(
        '~/machine_learning/affiliate-desirability/experimentation/original-datasets/Final_Undesirability_Test_Dataset_05_10_2018.csv')


    undesirables = original_dataframe.loc[original_dataframe['Undesirable'] == 1]

    desirability_scores = undesirables['Desirability_Score'].values

    bucket_1_limit = -3
    bucket_2_limit = -6

    undesirability_class_list = [0 if k >= bucket_1_limit else 1 if bucket_1_limit > k >= bucket_2_limit else 2 for k in desirability_scores]
    undesirables['Undesirability_class'] = undesirability_class_list

    undesirables.to_csv(
        '/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/ensemble_classification/second_layer_classification/Multiclass_Undesirability_Test_Dataset_05_16_2018.csv',
        index=None)

    return undesirables