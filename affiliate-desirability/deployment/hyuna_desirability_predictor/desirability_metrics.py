from sklearn.metrics import matthews_corrcoef, f1_score, precision_score, recall_score, confusion_matrix, roc_curve, auc
from sklearn.metrics import classification_report, accuracy_score

def evaluate_predictions(predictions, labels_test):

    # Out of all the users tagged as SU. What percentage of them was truly a SU
    precision = precision_score(labels_test, predictions)
    # Out of all the real SU. What percentage of them was tagged as being SU.
    recall = recall_score(labels_test, predictions)

    conf_mtrx = confusion_matrix(labels_test, predictions)
    #
    f1score = f1_score(labels_test, predictions)
    # Addapted to highly unbalanced classes
    mats_coefficient = matthews_corrcoef(labels_test, predictions)

    return precision, recall, conf_mtrx, f1score, mats_coefficient


def area_under_curve(predictions, labels_test):
    # Area under the curve
    fpr, tpr, thresholds = roc_curve(labels_test, predictions)
    roc_auc = auc(fpr, tpr)

    return roc_auc
