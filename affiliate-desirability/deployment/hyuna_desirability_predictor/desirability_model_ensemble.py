import pandas as pd
import numpy as np
# Model modules
import desirability_extraTrees as xt
import desirability_boosting as bt
import desirability_randomforest as rf
# Helper modules
import desirability_result_creator as drc
import desirability_metrics as mt
from sklearn.metrics import classification_report, accuracy_score
from pprint import pprint as pp
import os
import errno

### Global Variables

# The following flag defines whether the models will consider 6 or 36 days of data. Customize accordingly
# If true the ensemble will consider 6 days, otherwise it will consider 36 days
execute_ensemble_six_day_time_period = False

# Change the following path to point to the desired testing set (make sure to copy the mentioned testing set into
# the project first)
path_to_data_set = 'datasets/Desirability_Test_Dataset_05_10_2018.csv'

# If the following flag is set to True the script will create a 'combined results' table and save it to the 'results'
# folder, If set to flase then no combined results table will be created
create_combined_results = False

# The following flag determines if the script is being used to predict or to test new models. This flag works
# automatically. DO NOT modify its value here it will work by itself depending on the presence of the
# ground truths column in the dataset used to either predict or test the performance of the ensemble.
is_predicting = False


### Main method of the script. This is the method that contains the main instructions to run the program
def main():
    # Load the test dataset (path tedined above as global variable)
    dataframe_test = pd.read_csv(path_to_data_set)

    # If the column 'Undesirable' exists that means that we are trying to test the performance of a new ensemble,
    # otherwise we are predicting.
    if 'Undesirable' in dataframe_test.columns:
        #Set flag accordingly
        is_predicting = True
        # Make sure that labels have numerical value
        dataframe_test.replace('Yes', 1, inplace=True)
        dataframe_test.replace('No', 0, inplace=True)
        # Extract labels
        labels_test = dataframe_test['Undesirable'].values

    # Drop unnecessary column (Desirability_Score)
    dataframe_test.drop(['Desirability_Score'], axis=1, inplace=True)
    results_dataframe = dataframe_test

    if execute_ensemble_six_day_time_period == True:
        ### Execute models for 6 days ###
        xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
        combined_results = execute_model_6_days_ensemble(dataframe_test, save_results=False)

        # Create results table for 6 days
        results_dataframe['Combined_6'] = combined_results
        results_dataframe['ExtraTrees_6'] = xt_predictions
        results_dataframe['ExtraTrees_Prob_6'] = xt_probabilities
        results_dataframe['Adaboost_6'] = bt_predictions
        results_dataframe['Adaboost_Prob_6'] = bt_probabilities
        results_dataframe['Randomforests_6'] = rf_predictions
        results_dataframe['Randomforests_Prob_6'] = rf_probabilities

        # Save results to 'results' folder
        results_dataframe.to_csv('results/6_days_results_table.csv', index=None)
    else:
        ### Execute models for 36 days ###
        xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
        combined_results = execute_model_36_days_ensemble(dataframe_test, save_results=False)

        # Create results table for 36 days
        results_dataframe['Combined_36'] = combined_results
        results_dataframe['ExtraTrees_36'] = xt_predictions
        results_dataframe['ExtraTrees_Prob_36'] = xt_probabilities
        results_dataframe['Adaboost_36'] = bt_predictions
        results_dataframe['Adaboost_Prob_36'] = bt_probabilities
        results_dataframe['Randomforests_36'] = rf_predictions
        results_dataframe['Randomforests_Prob_36'] = rf_probabilities

        # Save results to 'results' folder
        results_dataframe.to_csv('results/36_days_results_table.csv', index=None)


    # Evaluate ensemble performance
    if is_predicting == True:
        evaluate_ensemble_performance(labels_test, combined_results)

    if create_combined_results == True:
        create_combined_results_table(dataframe_test)


def execute_model_36_days_ensemble(dataframe_test, save_results=False):
    labels_test = dataframe_test['Undesirable'].values

    ### Extratrees
    # Load model from memory (from 'models' folder)
    extratrees = drc.load_model('models/model_36_days_ExtraTreesClassifier.sav')
    # Extract the raw values corresponding to the features defined for the model in question
    xt_testing_set = dataframe_test[list(xt.features)].values
    # Create predictions
    xt_predictions = xt.create_predictions(extratrees, xt_testing_set)
    # Create probability distributions
    xt_probabilities = xt.create_probability_distributions(extratrees, xt_testing_set)[:, 1]
    # Evaluate model performance

    ### AdaBoost
    # Load model from memory (from 'models' folder)
    adaboost = drc.load_model('models/model_36_days_AdaboostClassifier.sav')
    # Extract the raw values corresponding to the features defined for the model in question
    bt_testing_set = dataframe_test[list(bt.features)].values
    # Create predictions
    bt_predictions = bt.create_predictions(adaboost, bt_testing_set)
    # Create probability distributions
    bt_probabilities = bt.create_probability_distributions(adaboost, bt_testing_set)[:, 1]
    # Evaluate model performance

    ### Randomforests
    # Load model from memory (from 'models' folder)
    randomforests = drc.load_model('models/model_36_days_RandomForestClassifier.sav')
    # Extract the raw values corresponding to the features defined for the model in question
    rf_testing_set = dataframe_test[list(rf.features)].values
    # Create predictions
    rf_predictions = rf.create_predictions(randomforests, rf_testing_set)
    # Create probability distributions
    rf_probabilities = rf.create_probability_distributions(randomforests, rf_testing_set)[:, 1]

    # Evaluate predictions
    if is_predicting == True:
        xt.evaluate_model_performance(extratrees, xt_predictions, labels_test, save_results_file=save_results)
        bt.evaluate_model_performance(adaboost, bt_predictions, labels_test, save_results_file=save_results)
        rf.evaluate_model_performance(randomforests, rf_predictions, labels_test, save_results_file=save_results)

    # Create combined results
    combined_results = []
    for i, val in enumerate(xt_predictions):
        combination = 0
        addedup = val + bt_predictions[i] + rf_predictions[i]
        if int(addedup) > 1:
            combination = 1
        else:
            combination = 0

        combined_results.append(combination)

    return xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
           combined_results


def execute_model_6_days_ensemble(dataframe_test, save_results=False):
    labels_test = dataframe_test['Undesirable'].values

    # Extratrees
    extratrees = drc.load_model('models/model_6_days_ExtraTreesClassifier.sav')
    xt_testing_set = dataframe_test[list(xt.features)].values
    xt_predictions = xt.create_predictions(extratrees, xt_testing_set)
    xt_probabilities = xt.create_probability_distributions(extratrees, xt_testing_set)[:, 1]

    # AdaBoost
    adaboost = drc.load_model('models/model_6_days_AdaboostClassifier.sav')
    bt_testing_set = dataframe_test[list(bt.features)].values
    bt_predictions = bt.create_predictions(adaboost, bt_testing_set)
    bt_probabilities = bt.create_probability_distributions(adaboost, bt_testing_set)[:, 1]

    # Randomforests
    randomforests = drc.load_model('models/model_6_days_RandomForestClassifier.sav')
    rf_testing_set = dataframe_test[list(rf.features)].values
    rf_predictions = rf.create_predictions(randomforests, rf_testing_set)
    rf_probabilities = rf.create_probability_distributions(randomforests, rf_testing_set)[:, 1]

    # Evaluate predictions
    if is_predicting == True:
        xt.evaluate_model_performance(extratrees, xt_predictions, labels_test, save_results_file=save_results)
        bt.evaluate_model_performance(adaboost, bt_predictions, labels_test, save_results_file=save_results)
        rf.evaluate_model_performance(randomforests, rf_predictions, labels_test, save_results_file=save_results)

    # Create combined results
    # combined_results = compute_combined_results(xt_predictions, bt_predictions, rf_predictions)
    combined_results = []
    for i, val in enumerate(xt_predictions):
        combination = 0
        addedup = val + bt_predictions[i] + rf_predictions[i]
        if int(addedup) > 1:
            combination = 1
        else:
            combination = 0

        combined_results.append(combination)

    return xt_predictions, xt_probabilities, bt_predictions, bt_probabilities, rf_predictions, rf_probabilities, \
           combined_results


def create_combined_results_table(dataframe_test):

    six_day_results_path = 'results/6_days_results_table.csv'

    if os.path.exists(six_day_results_path) == True:
        six_days_results_dataframe = pd.read_csv(six_day_results_path)
    else:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), six_day_results_path)

    thirtysix_day_results_path = 'results/36_days_results_table.csv'

    if os.path.exists(six_day_results_path) == True:
        thirtysix_days_results_dataframe = pd.read_csv(thirtysix_day_results_path)
    else:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), thirtysix_day_results_path)

    combined_results_dataframe = dataframe_test
    combined_results_dataframe['Combined_6'] = six_days_results_dataframe['Combined_6']
    combined_results_dataframe['Combined_36'] = thirtysix_days_results_dataframe['Combined_36']

    combined_results_dataframe['ExtraTrees_6'] = six_days_results_dataframe['ExtraTrees_6']
    combined_results_dataframe['ExtraTrees_Prob_6'] = six_days_results_dataframe['ExtraTrees_Prob_6']
    combined_results_dataframe['ExtraTrees_36'] = thirtysix_days_results_dataframe['ExtraTrees_36']
    combined_results_dataframe['ExtraTrees_Prob_36'] = thirtysix_days_results_dataframe['ExtraTrees_Prob_36']

    combined_results_dataframe['Adaboost_6'] = six_days_results_dataframe['Adaboost_6']
    combined_results_dataframe['Adaboost_Prob_6'] = six_days_results_dataframe['Adaboost_Prob_6']
    combined_results_dataframe['Adaboost_36'] = thirtysix_days_results_dataframe['Adaboost_36']
    combined_results_dataframe['Adaboost_Prob_36'] = thirtysix_days_results_dataframe['Adaboost_Prob_36']

    combined_results_dataframe['Randomforests_6'] = six_days_results_dataframe['Randomforests_6']
    combined_results_dataframe['Randomforests_Prob_6'] = six_days_results_dataframe['Randomforests_Prob_6']
    combined_results_dataframe['Randomforests_36'] = thirtysix_days_results_dataframe['Randomforests_36']
    combined_results_dataframe['Randomforests_Prob_36'] = thirtysix_days_results_dataframe['Randomforests_Prob_36']

    combined_results_dataframe.to_csv('results/combined_results_table.csv', index=None)


def compute_combined_results(xt_predictions, bt_predictions, rf_predictions):
    combined_results = []
    for i, val in enumerate(xt_predictions):
        combination = 0
        addedup = val + bt_predictions[i] + rf_predictions[i]
        if int(addedup) > 1:
            combination = 1
        else:
            combination = 0

    combined_results.append(combination)


def evaluate_ensemble_performance(labels_test, combined_results):
    # Print metrics (For optimization purposes)
    print('\nEnsemble_Classification_Results - Testing Confusion Matrix\n\n',
          pd.crosstab(labels_test, np.array(combined_results), rownames=['Actual'], colnames=['Predicted']))

    print('\nEnsemble_Classification_Results - Clasification report\n',
          classification_report(labels_test, np.array(combined_results)))

    print('\nEnsemble_Classification_Results - Test Accuracy\n',
          round(accuracy_score(labels_test, np.array(combined_results)), 3))

    # Apply desired metrics for model optimization
    precision, recall, conf_mtrx, f1score, mats_coefficient = mt.evaluate_predictions(np.array(combined_results),
                                                                                      labels_test)
    roc_auc = mt.area_under_curve(np.array(combined_results), labels_test)

    # Create file for model documentation
    file_name = 'Ensemble_Classification_Results_' + str(drc.now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['models'] = ['ExtraTrees', 'Adaboost', 'Randomforests']
    results_dictionary['six_day_time_period'] = execute_ensemble_six_day_time_period
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['precision-specificity(tp/tp+fp)'] = precision
    results_dictionary['recall-sensitivity(tp/tp+fn)'] = recall
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['area_under_curve'] = roc_auc
    pp(results_dictionary)


if __name__ == '__main__':
    main()
