import pandas as pd
import datetime
from sklearn.utils import shuffle

desirability_threshold = .25

def preprocess_dataframes(save=False):

    dataframe = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                            'ensemble_classification/second_layer_classification/'
                            'Multiclass_Undesirability_Train_Dataset_05_16_2018.csv')
    dataframe.to_csv(
        '~/machine_learning/affiliate-desirability/experimentation/original-datasets/Final_Undesirability_Train_Dataset_05_10_2018.csv',
        index=None)

    dataframe_test = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                            'ensemble_classification/second_layer_classification/'
                            'Multiclass_Undesirability_Test_Dataset_05_16_2018.csv')

    df_copy = pd.read_csv('~/machine_learning/affiliate-desirability/experimentation/'
                            'ensemble_classification/second_layer_classification/'
                            'Multiclass_Undesirability_Train_Dataset_05_16_2018.csv')
    df_copy.drop(['affiliate_id', 'affiliate_type', 'Desirability_Score', 'Undesirable', 'Undesirability_class'],
                 axis=1, inplace=True)

    normalization_coeffs = normalization_coefficients(df_copy, df_copy.columns)

    dataframe_normalized = normalize_values(dataframe, df_copy.columns, normalization_coeffs)
    dataframe_normalized = shuffle(dataframe_normalized)

    dataframe_test_normalized = normalize_values(dataframe_test, df_copy.columns, normalization_coeffs)
    dataframe_test_normalized = shuffle(dataframe_test_normalized)

    if save == True:
        dataframe_normalized_path = '~/machine_learning/affiliate-desirability/experimentation/' \
                                    'ensemble_classification/second_layer_classification/' \
                                    'Multiclass_Normalized_Train_Undesirability_Dataset_' + str(datetime.datetime.now()) + '.csv'

        dataframe_normalized.to_csv(dataframe_normalized_path, index=None)

        dataframe_test_normalized_path = '~/machine_learning/affiliate-desirability/experimentation/' \
                                    'ensemble_classification/second_layer_classification/' \
                                    'Multiclass_Normalized_Test_Undesirability_Dataset_' + str(datetime.datetime.now()) + '.csv'
        dataframe_test_normalized.to_csv(dataframe_test_normalized_path, index=None)

    return dataframe_normalized, dataframe_test_normalized


def normalization_coefficients(base_dataFrame, feature_list):
    max_list = []
    min_list = []
    for feature_name in feature_list:
        max_list.append(max(list(base_dataFrame[feature_name].values)))
        min_list.append(min(list(base_dataFrame[feature_name].values)))

    return list(zip(max_list, min_list))


def normalize_values(dataframe, feature_list, normalization_coeffs):
    # Create new dataframe & Save features to be left intact
    normalized_dataframe = pd.DataFrame()
    normalized_dataframe['affiliate_id'] = dataframe['affiliate_id']
    normalized_dataframe['affiliate_type'] = dataframe['affiliate_type']

    # Clean dataframe
    dataframe.drop(['affiliate_id', 'affiliate_type'], axis=1, inplace=True)

    for i, feature_name in enumerate(feature_list):
        values = list(dataframe[feature_name].values)
        max = normalization_coeffs[i][0]
        min = normalization_coeffs[i][1]

        for j, value in enumerate(values):
            normalized_value = (value - min) / (max - min)
            # clip values between 0 and 1
            if normalized_value < 0:
                normalized_value = 0
            elif normalized_value > 1:
                normalized_value = 1

            values[j] = normalized_value

        normalized_dataframe[feature_name] = values

    # Append these features to the end to keep the order of the DFs
    normalized_dataframe['Desirability_Score'] = dataframe['Desirability_Score']
    normalized_dataframe['Undesirable'] = dataframe['Undesirable']
    normalized_dataframe['Undesirability_class'] = dataframe['Undesirability_class']

    return normalized_dataframe


def downsample_balance_dataset(dataframe):
    # Shape (145, 72)
    undesirable_dataFrame = dataframe.loc[dataframe['Undesirable'] == 1]

    # Shape (696, 72)
    non_undesirable_dataFrame = dataframe.loc[dataframe['Undesirable'] == 0]
    #shape (145, 72)
    non_undesirable_dataFrame = non_undesirable_dataFrame[0:undesirable_dataFrame.shape[0]]

    balanced_dataFrame = pd.concat([undesirable_dataFrame, non_undesirable_dataFrame])

    balanced_dataFrame = shuffle(balanced_dataFrame)

    labels = balanced_dataFrame['Undesirable'].values

    return balanced_dataFrame, labels


def upsample_balance_dataset(dataframe):
    # Shape (145, 72)
    undesirable_dataframe = dataframe.loc[dataframe['Undesirable'] == 1]
    # Shape (696, 72)
    non_undesirable_dataframe = dataframe.loc[dataframe['Undesirable'] == 0]

    coeff = int(int(non_undesirable_dataframe.shape[0]) / int(undesirable_dataframe.shape[0]))
    coeff = int(coeff/2)

    augmented_undesirable_dataframe = pd.DataFrame()
    for i in range(coeff + 1):
        augmented_undesirable_dataframe = pd.concat([augmented_undesirable_dataframe, undesirable_dataframe])

    augmented_undesirable_dataframe = augmented_undesirable_dataframe[0:non_undesirable_dataframe.shape[0]]

    balanced_dataFrame = pd.concat([augmented_undesirable_dataframe, non_undesirable_dataframe])
    balanced_dataFrame = shuffle(balanced_dataFrame)

    labels = balanced_dataFrame['Undesirable'].values

    return balanced_dataFrame, labels


def create_train_set_for_second_layer_classification():
    # Train
    # original_dataframe = pd.read_csv(
    #     '~/machine_learning/affiliate-desirability/experimentation/original-datasets/Final_Undesirability_Train_Dataset_05_10_2018.csv')

    # Test
    original_dataframe = pd.read_csv(
        '~/machine_learning/affiliate-desirability/experimentation/original-datasets/Final_Undesirability_Test_Dataset_05_10_2018.csv')


    undesirables = original_dataframe.loc[original_dataframe['Undesirable'] == 1]

    desirability_scores = undesirables['Desirability_Score'].values

    bucket_1_limit = -3
    bucket_2_limit = -6

    undesirability_class_list = [0 if k >= bucket_1_limit else 1 if bucket_1_limit > k >= bucket_2_limit else 2 for k in desirability_scores]
    undesirables['Undesirability_class'] = undesirability_class_list

    undesirables.to_csv(
        '/Users/leopoldogv/machine_learning/affiliate-desirability/experimentation/ensemble_classification/second_layer_classification/Multiclass_Undesirability_Test_Dataset_05_16_2018.csv',
        index=None)

    return undesirables