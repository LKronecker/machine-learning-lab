Welcome to desirability predictor for Hyuna!

### Introduction

In this project you will find an ensemble of models that will predict member desirability using three different
statistical predictive model approaches. The ensemble will create predictions based on 6 days, 36 days and both
combined on a changeable dataset.

### Installation

To be able to run the project wou will have to install Python 3.6 or higher in your system. Follow the following
instructions depending on your Operating System:

* Installing Python 3 on Mac OS X - http://docs.python-guide.org/en/latest/starting/install3/osx/
* Installing Python 3 on Windows - http://docs.python-guide.org/en/latest/starting/install3/win/

Once that Python 3 is installed in your system you will need to install the dependencies listed in the requrements.txt
file that lives in this same directory. You can do depending on how you manage your work environment.

If you are installing dependencies directly on your system the following command should work (in your command line
tool):  pip install -r requirements.txt (Make sure you are in the directory to the requirements file)

Otherwise if you are using Conda the following command should work: conda install --yes --file requirements.txt
(While having your work environment activated and while being on the requirements file path)

### Execution

Once the installation requirements are fulfilled  you will be able to execute the project from an IDE of from your
command line tool itself. The script that builds the model ensemble and that predicts desirability using the ensemble
scheme has *** desirability_model_ensemble.py *** as file name.

If you are going to execute it through the command line tool the command: ***python3 desirability_model_ensemble.py ***
will do the trick. Otherwise if you are using an IDE you will running using the built in Run command in your IDE.


### Customization

The main script (desirability_model_ensemble.py) is customizable so that the dataset to predict on can be easily changed
At the time it is set to predict on the test set in the path *** datasets/Desirability_Test_Dataset_05_10_2018.csv ***
If you wish to change the dataset on which the prediction takes place you will have to paste the new dataset into the
*** datasets *** folder and then open the main script and change the *** path_to_test_set *** global variable to point
to the dataset that you want to predict on.

Once you execute the main scrip it will create predictions depending on the variable
*** execute_ensemble_six_day_time_period *** defined at the top of the main script. If it is set to 'True' the ensemble
will consider 6 days, otherwise it will consider 36 days. A results table will be crated for each one of the periods
of time (6 and 36 days), the corresponding table will be stored into the *** results *** folder.

The main script suports creating a combined results table (for 6 and 36 days combined). In order to do you you will
have to run it twice. first with *** execute_ensemble_six_day_time_period *** set to True and then set to False.
On the second run you will have to set *** create_combined_results *** to True so that the combined results table
is created and stored into the results folder.

If you explore the script and the structure of the project you will realize that there is a folder named *models*
these are the pre-trained models that the script is using to predict on the given data. You can easily retrain each
one of the models by running the model scripts independently *** desirability_boosting.py, desirability_extraTrees.py,
desirability_randomforest.py *** Each time one of these scripts is run it willl create a new model and save it in the
* models * folder. You can optimize the model results by modyfing the model's hyperparameters at the top of each one
of the scripts and/or by changing the training and testing sets. To change the training and/or testing sets you will
have to copy the corresponding *.csv files into the project and modify the path to load the appropiate file inside
each one of the training scripts.

IMPORTANT NOTE: The main script * desirability_model_ensemble.py * serves a double purpose: It can be used to predict
on affiliate data points and also to test the performance of the ensemble given a testing set. The script is set up in
a way such that is the data set passed (path assigned at the variable 'path_to_data_set' at the top of the script)
contains the column called 'Undesirable' then the script will detect it and it will test using the given ground truths.
On the other hand is the dataset passed does not contain the column 'Undesirable' the script will only predict on the
given data points and create the corresponding result files.

Once you retrain the models (this is not necesary, the ensemble should work as it is) you can run the main script again
build results using the new models.

Happy predicting!

- Mat. Leopoldo G Vargas
