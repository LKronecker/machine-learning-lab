import json
import pickle
import datetime
now = datetime.datetime.now()

def create_results_file(file_name, results_dictionary):
    with open(file_name, 'w') as outfile:
        json.dump(results_dictionary, outfile)


def save_model(model, model_name):
    filename = model_name + '.sav'
    pickle.dump(model, open(filename, 'wb'))


def load_model(filename):
    loaded_model = pickle.load(open(filename, 'rb'))
    return loaded_model


