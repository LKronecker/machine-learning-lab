from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from pprint import pprint as pp
import desirability_preprocessing as dp
import desirability_result_creator as drc
import desirability_metrics as mt

import numpy as np

prediction_threshold = .5
seed = 260
np.random.seed(seed)

# The following flag defines whether the models will consider 6 or 36 days of data. Customize accordingly
# If true the ensemble will consider 6 days, otherwise it will consider 36 days
six_day_time_period = False

# Paths to test and training sets
path_to_training_set = 'datasets/Desirability_Train_Dataset_05_10_2018.csv'
path_to_test_set = 'datasets/Desirability_Test_Dataset_05_10_2018.csv'

# Model Hyper parameters. Optimize here if desired
max_depth = 2
criterion = 'gini'
algorithm = "SAMME"
n_estimators = 5000
learning_rate = 0.05

if six_day_time_period == True:
    features = ['Net_Setttlm_total_6', 'Gross_Margin_6', 'BR_mmb_rate', 'cancel_rate_1', 'Gross_Setttlm_6',
                'SPU_sales_ratio', 'PU_conversion_rate', 'Total_Cost', 'Processing_Cost_6', 'avg_net_rev_6',
                'sales_PU_ratio']

else:
    features = ['Gross_Margin_36', 'total_cancel_rate_36', 'cancel_rate_36', 'Total_Cost', 'cancel_rate_1',
                'Net_Setttlm_36', 'CA_mmb_rate', 'rev_growth_rate_36', 'rev_VS_cost_36', 'GM_growth_rate_36',
                'avg_net_rev_36']


def main():
    dataframe = dp.pd.read_csv(path_to_training_set)
    dataframe_test = dp.pd.read_csv(path_to_test_set)

    labels_train = dataframe['Undesirable'].values
    labels_test = dataframe_test['Undesirable'].values

    training_set = dataframe[list(features)].values
    testing_set = dataframe_test[list(features)].values

    adaboost = train_model(training_set, labels_train)

    predictions = create_predictions(adaboost, testing_set)
    evaluate_model_performance(adaboost, predictions, labels_test, save_results_file=False)

    if six_day_time_period == True:
        model_name = 'models/model_6_days_AdaboostClassifier'
    else:
        model_name = 'models/model_36_days_AdaboostClassifier'

    # Save model to 'models' folder
    drc.save_model(adaboost, model_name)


def train_model(features_train, labels_train):
    dTree = DecisionTreeClassifier(max_depth=max_depth, criterion=criterion)

    adaboost = AdaBoostClassifier(base_estimator=dTree, algorithm=algorithm, n_estimators=n_estimators,
                                  learning_rate=learning_rate, random_state=seed)

    adaboost.fit(features_train, labels_train)
    pp(adaboost)

    return adaboost


def create_predictions(model, features_test):
    probabilities = create_probability_distributions(model, features_test)
    predictions_filtered = np.array([1 if probabilities[:, 1][k] > prediction_threshold else 0 for k in
                                     range(len(probabilities[:, 1]))])
    return predictions_filtered

def create_probability_distributions(model, features_test):
    probabilities = model.predict_proba(features_test)
    return probabilities


def evaluate_model_performance(model, predictions, labels_test, save_results_file=False):
    precision, recall, conf_mtrx, f1score, mats_coefficient = mt.evaluate_predictions(predictions, labels_test)
    roc_auc = mt.area_under_curve(predictions, labels_test)

    # Print metrics
    print('\nAdaboost - Testing Confusion Matrix\n\n', dp.pd.crosstab(labels_test, predictions,
                                                                   rownames=['Actuall'], colnames=['Predicted']))

    print('\nAdaboost - Clasification report\n', mt.classification_report(labels_test, predictions))

    print('\nAdaboost - Test Accuracy\n', round(mt.accuracy_score(labels_test, predictions), 3))


    # Create file for model documentation
    file_name = 'Classification_Results_' + str(drc.now.strftime("%Y-%m-%d %H-%M")) + '.json'

    results_dictionary = {}
    results_dictionary['experiment'] = file_name
    results_dictionary['normalized_data'] = False
    results_dictionary['short_time_period'] = six_day_time_period
    results_dictionary['model_name'] = str(type(model))
    results_dictionary['normalized_data'] = False
    results_dictionary['number_of_testing_samples'] = len(labels_test)
    results_dictionary['features_used'] = features
    results_dictionary['hyperparameters'] = {
        'base_estimator': {'name': 'DecisionTreeClassifier',
                           'max_depth': max_depth},
        'algorithm': algorithm,
        'learning_rate': 0.05,
        'n_estimators': n_estimators,
        'random_state': seed}

    results_dictionary['precision-specificity(tp/tp+fp)'] = precision
    results_dictionary['recall-sensitivity(tp/tp+fn)'] = recall
    results_dictionary['confusion_matrix'] = {'matrix': conf_mtrx.tolist(),
                                              'matrix_desc': ['[TN, FP]', '[FN, TP]'],
                                              'matrix_acc': (conf_mtrx.tolist()[0][0] + conf_mtrx.tolist()[1][1]) / len(
                                                  labels_test)}
    results_dictionary['f1_score'] = f1score
    results_dictionary['matthews_correlation_coefficient'] = mats_coefficient
    results_dictionary['prediction_threshold'] = prediction_threshold
    results_dictionary['area_under_curve'] = roc_auc

    pp(results_dictionary)

    if save_results_file == True:
        drc.create_results_file(file_name, results_dictionary)


if __name__ == '__main__':
    main()